﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.Extention
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using System.Collections.Specialized;
using System.Text;

namespace Pilot.Transport
{
  internal static class Extention
  {
    internal static string Parse(this NameValueCollection collection)
    {
      var stringBuilder = new StringBuilder("{");
      foreach (var allKey in collection.AllKeys)
        stringBuilder.AppendFormat("\"{0}\":\"{1}\",", (object) allKey, (object) collection[allKey]);
      stringBuilder.Remove(stringBuilder.Length - 1, 1);
      stringBuilder.Append("}");
      return stringBuilder.ToString();
    }
  }
}
