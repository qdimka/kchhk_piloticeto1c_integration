﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.Session
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using log4net;
using System;
using System.Globalization;
using System.Net;
using System.Threading;

namespace Pilot.Transport
{
  internal class Session : ISessionCallback
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof (Session));
    private readonly CallbackQueue _callbackQueue = new CallbackQueue();
    private string _token = string.Empty;
    private readonly ISession _session;
    private readonly int _callbackTimeout;
    private readonly int _sessionTimeout;
    private readonly Guid _id;
    private volatile Timer _callbackTimer;
    private readonly Timer _sessionTimer;
    private readonly OnSessionTimeout _onSessionTimeout;
    private long _isClosed;
    private long _isCalling;
    private readonly ISessionRegistry _registry;

    public Guid Id => _id;

    public Session(ISessionRegistry registry, int callbackTimeout, int sessionTimeout, OnSessionTimeout onSessionTimeout, IMarshallingFactory marshallingFactory, IPEndPoint endPoint)
    {
      _callbackTimeout = callbackTimeout;
      _sessionTimeout = sessionTimeout;
      _onSessionTimeout = onSessionTimeout;
      _registry = registry;
      _id = Guid.NewGuid();
      _session = registry.NewSession((ISessionCallback) this, marshallingFactory, endPoint);
      _sessionTimer = new Timer(new TimerCallback(SessionTimeout), (object) null, sessionTimeout, -1);
    }

    public void Close(ISessionRegistry registry)
    {
      if (Interlocked.Increment(ref _isClosed) > 1L)
        return;
      while (Interlocked.Read(ref _isCalling) > 0L)
        Thread.Sleep(1);
      registry.CloseCallback(_session, _callbackQueue.GetData());
      registry.CloseSession(_session);
      var responseInfo = _callbackQueue.FinishWaiting();
      SendBye(responseInfo.Context, responseInfo.Packet);
      _callbackTimer = (Timer) null;
      _sessionTimer.Change(-1, -1);
    }

    public void ProcessCallRequest(Context context)
    {
      if (Interlocked.Read(ref _isClosed) > 0L)
        throw new TransportException("Session was closed");
      Interlocked.Increment(ref _isCalling);
      try
      {
        RestartSessionTimer();
        var data = _session.Call(context.GetInput());
        context.Send(data);
        if (!(_token != _session.Token))
          return;
        RestoreDumpCallbacks();
      }
      finally
      {
        Interlocked.Decrement(ref _isCalling);
      }
    }

    public void ProcessGetRequest(Context context)
    {
      if (Interlocked.Read(ref _isClosed) > 0L)
        throw new TransportException("Session was closed");
      Interlocked.Increment(ref _isCalling);
      try
      {
        RestartSessionTimer();
        var data = _session.Call(context.GetKeys());
        context.Send(data);
      }
      finally
      {
        Interlocked.Decrement(ref _isCalling);
      }
    }

    public void Callback(byte[] data)
    {
      StopCallbackTimer();
      Send(_callbackQueue.NewPacket(data));
    }

    public void ProcessCallbackRequest(Context context)
    {
      if (Interlocked.Read(ref _isClosed) > 0L)
        throw new TransportException("Session was closed");
      RestartSessionTimer();
      var response = _callbackQueue.NewContext(context);
      if (response.Context != context)
        StartCallbackTimer(context);
      Send(response);
    }

    private void RestoreDumpCallbacks()
    {
      _token = _session.Token;
      var numArray = _registry.RestoreCallback(_session);
      if (numArray == null)
        return;
      foreach (var data in numArray)
        _callbackQueue.AddPacket(data);
    }

    private void StartCallbackTimer(Context context)
    {
      _callbackTimer = new Timer(new TimerCallback(TimerCallback), (object) context, _callbackTimeout, -1);
    }

    private void StopCallbackTimer()
    {
      var callbackTimer = _callbackTimer;
      _callbackTimer = (Timer) null;
      if (callbackTimer == null)
        return;
      callbackTimer.Change(-1, -1);
    }

    private void TimerCallback(object state)
    {
      Send(_callbackQueue.ContextTimeout((Context) state));
    }

    private void Send(ResponseInfo response)
    {
      Send(response.Context, response.Packet);
    }

    private void Send(Context context, CallbackPacket packet)
    {
      if (context == null)
        return;
      try
      {
        if (packet == null)
        {
          context.Send();
        }
        else
        {
          context.SetCookie("CBID", packet.Id.ToString((IFormatProvider) CultureInfo.InvariantCulture));
          context.Send(packet.Data);
        }
      }
      catch (HttpListenerException ex)
      {
        Logger.Error((object) "We can do nothing here.", (Exception) ex);
      }
      catch (ObjectDisposedException ex)
      {
        Logger.Error((object) "We can do nothing here.", (Exception) ex);
      }
      catch (Exception ex1)
      {
        try
        {
          context.Send(ex1);
        }
        catch (Exception ex2)
        {
          Logger.Error((object) "We can do nothing here.", ex2);
        }
      }
    }

    private void SendBye(Context context, CallbackPacket packet)
    {
      if (context == null)
        return;
      try
      {
        context.BadSession();
      }
      catch (Exception ex)
      {
        Logger.Error((object) "We can do nothing here.", ex);
      }
    }

    private void RestartSessionTimer()
    {
      _sessionTimer.Change(_sessionTimeout, -1);
    }

    private void SessionTimeout(object state)
    {
      _onSessionTimeout(this);
    }

    public delegate void OnSessionTimeout(Session session);
  }
}
