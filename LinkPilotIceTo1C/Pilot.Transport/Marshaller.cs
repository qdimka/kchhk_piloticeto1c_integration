﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.Marshaller
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

namespace Pilot.Transport
{
  public class Marshaller : IGetService
  {
    private readonly ICallService _service;

    public Marshaller(ICallService service)
    {
      _service = service;
    }

    public T Get<T>() where T : class
    {
      return new MarshallerProxy<T>(_service).GetTransparentProxy() as T;
    }
  }
}
