﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.MarshallerProxy`1
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using System;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;

namespace Pilot.Transport
{
  internal class MarshallerProxy<T> : RealProxy where T : class
  {
    private readonly ICallService _service;

    public MarshallerProxy(ICallService service)
      : base(typeof (T))
    {
      _service = service;
    }

    public override IMessage Invoke(IMessage msg)
    {
      var methodCallMessage = (IMethodCallMessage) msg;
      var methodBase = (MethodInfo) methodCallMessage.MethodBase;
      if (methodBase.Name == "GetHashCode")
        return (IMessage) new ReturnMessage((object) GetHashCode(), (object[]) null, 0, methodCallMessage.LogicalCallContext, methodCallMessage);
      if (methodBase.Name == "Equals")
        return (IMessage) new ReturnMessage((object) Equals(methodCallMessage.Args[0]), (object[]) null, 0, methodCallMessage.LogicalCallContext, methodCallMessage);
      if (methodBase.Name == "ToString")
        return (IMessage) new ReturnMessage((object) ToString(), (object[]) null, 0, methodCallMessage.LogicalCallContext, methodCallMessage);
      try
      {
        return DataToResult(_service.Call(CallToData(methodCallMessage, methodBase)), methodCallMessage, methodBase);
      }
      catch (Exception ex)
      {
        if (ex is TargetInvocationException && ex.InnerException != null)
          return (IMessage) new ReturnMessage(ex.InnerException, msg as IMethodCallMessage);
        return (IMessage) new ReturnMessage(ex, msg as IMethodCallMessage);
      }
    }

    private byte[] CallToData(IMethodCallMessage methodCall, MethodInfo method)
    {
      using (var memoryStream = new MemoryStream())
      {
        var marshallingMessage = new MarshallingMessage()
        {
          Interface = method.DeclaringType.Name,
          Method = method.Name,
          ParamCount = methodCall.InArgCount
        };
        for (var index = 0; index < methodCall.InArgCount; ++index)
        {
          if (methodCall.InArgs[index] == null)
            marshallingMessage.NullParamIndices.Add(index);
        }
        ProtoSerializer.Serialize((Stream) memoryStream, (object) marshallingMessage);
        foreach (var inArg in methodCall.InArgs)
        {
          if (inArg != null)
            ProtoSerializer.Serialize((Stream) memoryStream, inArg);
        }
        return memoryStream.ToArray();
      }
    }

    private IMessage DataToResult(byte[] data, IMethodCallMessage methodCall, MethodInfo method)
    {
      if (data.Length == 0)
        return (IMessage) new ReturnMessage((object) null, (object[]) null, 0, methodCall.LogicalCallContext, methodCall);
      using (var memoryStream = new MemoryStream(data))
        return (IMessage) new ReturnMessage(ProtoSerializer.Deserialize((Stream) memoryStream, method.ReturnType), (object[]) null, 0, methodCall.LogicalCallContext, methodCall);
    }
  }
}
