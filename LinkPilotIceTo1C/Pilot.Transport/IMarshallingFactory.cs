﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.IMarshallingFactory
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

namespace Pilot.Transport
{
  public interface IMarshallingFactory
  {
    ICallService GetUnmarshaller(IImplementationFactory implementationFactory);

    IGetService GetMarshaller(ICallService callService);
  }
}
