﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.Unmarshaller
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Pilot.Transport
{
  public class Unmarshaller : ICallService
  {
    private readonly ConcurrentDictionary<string, object> _registered = new ConcurrentDictionary<string, object>();
    private readonly IImplementationFactory _factory;

    public Unmarshaller(IImplementationFactory factory)
    {
      _factory = factory;
    }

    public byte[] Call(byte[] data)
    {
      using (var memoryStream = new MemoryStream(data))
      {
        var marshallingMessage = ProtoSerializer.Deserialize<MarshallingMessage>((Stream) memoryStream);
        var implementation = GetImplementation(marshallingMessage.Interface);
        var method = implementation.GetType().GetMethod(marshallingMessage.Method);
        var parameters1 = method.GetParameters();
        if (((IEnumerable<ParameterInfo>) parameters1).Count<ParameterInfo>() != marshallingMessage.ParamCount && ((IEnumerable<ParameterInfo>) parameters1).Count<ParameterInfo>((Func<ParameterInfo, bool>) (p => !p.IsOptional)) != marshallingMessage.ParamCount)
          throw new InvalidDataException(string.Format("Method {0}.{1} received {2} parameters, but {3} required", (object) marshallingMessage.Interface, (object) marshallingMessage.Method, (object) marshallingMessage.ParamCount, (object) parameters1.Length));
        var parameters2 = new object[((IEnumerable<ParameterInfo>) parameters1).Count<ParameterInfo>()];
        for (var index = 0; index < ((IEnumerable<ParameterInfo>) parameters1).Count<ParameterInfo>(); ++index)
        {
          if (!marshallingMessage.NullParamIndices.Contains(index))
          {
            parameters2[index] = ProtoSerializer.Deserialize((Stream) memoryStream, parameters1[index].ParameterType);
            if (parameters1[index].IsOptional && parameters2[index] == null)
              parameters2[index] = Type.Missing;
          }
        }
        try
        {
          return ResultToBytes(method.Invoke(implementation, parameters2));
        }
        catch (Exception ex)
        {
          if (ex is TargetInvocationException && ex.InnerException != null)
            throw ex.InnerException;
          throw;
        }
      }
    }

    private byte[] ResultToBytes(object result)
    {
      if (result == null)
        return new byte[0];
      using (var memoryStream = new MemoryStream())
      {
        ProtoSerializer.Serialize((Stream) memoryStream, result);
        return memoryStream.ToArray();
      }
    }

    private object GetImplementation(string interfaceName)
    {
      return _registered.GetOrAdd(interfaceName, (Func<string, object>) (iName => _factory.GetImplementation(iName)));
    }
  }
}
