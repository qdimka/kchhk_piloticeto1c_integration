﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.CallbackQueue
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pilot.Transport
{
  internal class CallbackQueue
  {
    private object _locker = new object();
    private Queue<CallbackPacket> _packets = new Queue<CallbackPacket>();
    private Context _waiting;
    private long _cbid;

    public ResponseInfo FinishWaiting()
    {
      var responseInfo = new ResponseInfo((Context) null, (CallbackPacket) null);
      lock (_locker)
      {
        if (_waiting != null)
        {
          responseInfo.Context = _waiting;
          _waiting = (Context) null;
          if (_packets.Count > 0)
            responseInfo.Packet = _packets.Peek();
        }
      }
      return responseInfo;
    }

    public ResponseInfo ContextTimeout(Context context)
    {
      var responseInfo = new ResponseInfo((Context) null, (CallbackPacket) null);
      lock (_locker)
      {
        if (context == _waiting)
        {
          responseInfo.Context = _waiting;
          _waiting = (Context) null;
          if (_packets.Count > 0)
            responseInfo.Packet = _packets.Peek();
        }
      }
      return responseInfo;
    }

    public void AddPacket(byte[] data)
    {
      lock (_locker)
        _packets.Enqueue(new CallbackPacket(++_cbid, data));
    }

    public ResponseInfo NewPacket(byte[] data)
    {
      var responseInfo = new ResponseInfo((Context) null, (CallbackPacket) null);
      lock (_locker)
      {
        _packets.Enqueue(new CallbackPacket(++_cbid, data));
        if (_waiting != null)
        {
          responseInfo.Context = _waiting;
          _waiting = (Context) null;
          responseInfo.Packet = _packets.Peek();
        }
      }
      return responseInfo;
    }

    public ResponseInfo NewContext(Context context)
    {
      var responseInfo = new ResponseInfo((Context) null, (CallbackPacket) null);
      lock (_locker)
      {
        RemoveAcceptedCallbacks(context);
        if (_packets.Count == 0)
        {
          responseInfo.Context = _waiting;
          _waiting = context;
        }
        else
        {
          responseInfo.Packet = _packets.Peek();
          if (_waiting == null)
          {
            responseInfo.Context = context;
          }
          else
          {
            responseInfo.Context = _waiting;
            _waiting = context;
          }
        }
      }
      return responseInfo;
    }

    public byte[][] GetData()
    {
      lock (_locker)
        return _packets.Select<CallbackPacket, byte[]>((Func<CallbackPacket, byte[]>) (p => p.Data)).ToArray<byte[]>();
    }

    private long GetCallbackId(Context context)
    {
      var cookie = context.GetCookie("CBID");
      long result;
      if (string.IsNullOrEmpty(cookie) || !long.TryParse(cookie, out result))
        return 0;
      return result;
    }

    private void RemoveAcceptedCallbacks(Context context)
    {
      var callbackId = GetCallbackId(context);
      while (_packets.Count > 0 && _packets.Peek().Id <= callbackId)
        _packets.Dequeue();
    }
  }
}
