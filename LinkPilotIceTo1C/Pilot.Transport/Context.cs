﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.Context
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using ProtoBuf;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Pilot.Transport
{
  internal class Context
  {
    private readonly HttpListenerContext _httpContext;

    public string Method => _httpContext.Request.Url.AbsolutePath;

    public Context(HttpListenerContext httpContext)
    {
      _httpContext = httpContext;
      _httpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
      _httpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    }

    public string GetCookie(string name)
    {
      var cookie = _httpContext.Request.Cookies[name];
      if (cookie == null)
        return string.Empty;
      return cookie.Value;
    }

    public void SetCookie(string name, string value)
    {
      _httpContext.Response.AppendCookie(new Cookie(name, value));
    }

    public void SetCookie(string name, string value, string path)
    {
      _httpContext.Response.AppendCookie(new Cookie(name, value, path));
    }

    public void SetCookie(string name, string value, string path, string domain)
    {
      _httpContext.Response.AppendCookie(new Cookie(name, value, path, domain));
    }

    public byte[] GetInput()
    {
      return _httpContext.Request.InputStream.ToByteArray();
    }

    public byte[] GetKeys()
    {
      return Encoding.Default.GetBytes(_httpContext.Request.QueryString.Parse());
    }

    public void BadSession()
    {
      _httpContext.Response.StatusCode = 400;
      _httpContext.Response.Close();
    }

    public void Send()
    {
      _httpContext.Response.StatusCode = 200;
      _httpContext.Response.Close();
    }

    public void Send(string message)
    {
      _httpContext.Response.ContentType = "text/plain";
      _httpContext.Response.StatusCode = 200;
      _httpContext.Response.SendAndClose(Encoding.UTF8.GetBytes(message), false);
    }

    public void Send(byte[] data)
    {
      _httpContext.Response.ContentType = "application/octet-stream";
      _httpContext.Response.StatusCode = 200;
      _httpContext.Response.ContentLength64 = (long) data.Length;
      _httpContext.Response.SendAndClose(data, false);
    }

    public void Send(Exception ex)
    {
      var exceptionData = GetExceptionData(ex);
      _httpContext.Response.ContentType = "application/octet-stream";
      _httpContext.Response.StatusCode = 500;
      _httpContext.Response.SendAndClose(exceptionData, false);
    }

    private static byte[] GetExceptionData(Exception exception)
    {
      using (var memoryStream = new MemoryStream())
      {
        try
        {
          Serializer.Serialize<ProtoExceptionInfo>((Stream) memoryStream, new ProtoExceptionInfo(exception));
          return memoryStream.ToArray();
        }
        catch (Exception ex)
        {
          return GetExceptionData((Exception) new InvalidOperationException(string.Format("Server error getting exception info: {0}", (object) ex.Message)));
        }
      }
    }

    public IPEndPoint GetRemoteEndPoint()
    {
      return _httpContext.Request.RemoteEndPoint;
    }
  }
}
