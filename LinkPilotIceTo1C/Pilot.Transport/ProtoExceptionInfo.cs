﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.ProtoExceptionInfo
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using ProtoBuf;
using System;

namespace Pilot.Transport
{
  [ProtoContract]
  public class ProtoExceptionInfo
  {
    [ProtoMember(1)]
    public string ExceptionMessage { get; set; }

    [ProtoMember(2)]
    public Type ExceptionType { get; set; }

    public ProtoExceptionInfo()
    {
    }

    public ProtoExceptionInfo(Exception e)
    {
      ExceptionMessage = e.Message;
      ExceptionType = e.GetType();
    }
  }
}
