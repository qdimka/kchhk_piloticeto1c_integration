﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.MarshallingFactory
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

namespace Pilot.Transport
{
  public class MarshallingFactory : IMarshallingFactory
  {
    private IGetService _marshaller;
    private ICallService _unmarshaller;

    public ICallService GetUnmarshaller(IImplementationFactory implementationFactory)
    {
      return _unmarshaller ?? (_unmarshaller = (ICallService) new Unmarshaller(implementationFactory));
    }

    public IGetService GetMarshaller(ICallService callService)
    {
      return _marshaller ?? (_marshaller = (IGetService) new Marshaller(callService));
    }
  }
}
