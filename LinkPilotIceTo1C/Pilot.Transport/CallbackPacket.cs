﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.CallbackPacket
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

namespace Pilot.Transport
{
  internal class CallbackPacket
  {
    private readonly long _id;
    private readonly byte[] _data;

    public long Id => _id;

    public byte[] Data => _data;

    public CallbackPacket(long id, byte[] data)
    {
      _id = id;
      _data = data;
    }
  }
}
