﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.JsonUnmarshaller
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Reflection;
using System.Text;

namespace Pilot.Transport
{
  internal class JsonUnmarshaller : ICallService
  {
    private readonly ConcurrentDictionary<string, object> _registered = new ConcurrentDictionary<string, object>();
    private readonly IImplementationFactory _factory;

    public JsonUnmarshaller(IImplementationFactory factory)
    {
      _factory = factory;
    }

    public byte[] Call(byte[] data)
    {
      var jobject = (JObject) JsonConvert.DeserializeObject(Encoding.Default.GetString(data));
      var marshallingMessage = new MarshallingMessage()
      {
        Interface = jobject.SelectToken("api").ToString(),
        Method = jobject.SelectToken("method").ToString(),
        ParamCount = jobject.Count - 2
      };
      var implementation = GetImplementation(marshallingMessage.Interface);
      var method = implementation.GetType().GetMethod(marshallingMessage.Method);
      var parameters1 = method.GetParameters();
      var parameters2 = new object[marshallingMessage.ParamCount];
      for (var index = 0; index < marshallingMessage.ParamCount; ++index)
      {
        var jtoken = jobject.SelectToken(parameters1[index].Name);
        var obj = jtoken.ToObject(parameters1[index].ParameterType);
        if (parameters1[index].Name == "protectedPassword")
          obj = Convert.ChangeType((object) jtoken.ToString().EncryptAes(), parameters1[index].ParameterType);
        parameters2[index] = obj;
      }
      try
      {
        var result = method.Invoke(implementation, parameters2);
        if (method.ReturnType.FullName == "System.Byte[]")
          return result as byte[];
        return ResultToBytes(result);
      }
      catch (Exception ex)
      {
        if (ex is TargetInvocationException && ex.InnerException != null)
          return Encoding.UTF8.GetBytes(ex.InnerException.Message);
        return Encoding.UTF8.GetBytes(ex.Message);
      }
    }

    private byte[] ResultToBytes(object result)
    {
      if (result == null)
        return new byte[0];
      return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(result));
    }

    private object GetImplementation(string interfaceName)
    {
      return _registered.GetOrAdd(interfaceName, (Func<string, object>) (iName => _factory.GetImplementation(iName)));
    }
  }
}
