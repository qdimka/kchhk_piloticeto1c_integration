﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.ProtoSerializer
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using ProtoBuf;
using ProtoBuf.Meta;
using System;
using System.IO;

namespace Pilot.Transport
{
  internal static class ProtoSerializer
  {
    public static void Serialize(Stream stream, object instance)
    {
      RuntimeTypeModel.Default.SerializeWithLengthPrefix((Stream) stream, instance, (Type) instance.GetType(), PrefixStyle.Base128, 0);
    }

    public static object Deserialize(Stream stream, Type type)
    {
      return RuntimeTypeModel.Default.DeserializeWithLengthPrefix((Stream) stream, (object) null, (Type) type, PrefixStyle.Base128, 0);
    }

    public static T Deserialize<T>(Stream stream)
    {
      return Serializer.DeserializeWithLengthPrefix<T>((Stream) stream, PrefixStyle.Base128, 0);
    }
  }
}
