﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.Extensions
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using System.IO;

namespace Pilot.Transport
{
  public static class Extensions
  {
    public static byte[] ToByteArray(this Stream stream)
    {
      using (stream)
      {
        using (var memoryStream = new MemoryStream())
        {
          stream.CopyTo((Stream) memoryStream);
          return memoryStream.ToArray();
        }
      }
    }
  }
}
