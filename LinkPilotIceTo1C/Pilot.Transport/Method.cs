﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.Method
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

namespace Pilot.Transport
{
  internal static class Method
  {
    public const string Root = "/";
    public const string Connect = "/connect";
    public const string Call = "/call";
    public const string Disconnect = "/disconnect";
    public const string Callback = "/callback";
    public const string WebConnect = "/web/connect";
    public const string WebCall = "/web/call";
    public const string WebFile = "/web/file";
  }
}
