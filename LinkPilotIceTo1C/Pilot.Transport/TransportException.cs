﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.TransportException
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using System;
using System.Runtime.Serialization;

namespace Pilot.Transport
{
  [Serializable]
  public class TransportException : Exception
  {
    public TransportException()
    {
    }

    public TransportException(string message)
      : base(message)
    {
    }

    public TransportException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    protected TransportException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}
