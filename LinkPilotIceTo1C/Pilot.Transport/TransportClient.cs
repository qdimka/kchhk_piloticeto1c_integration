﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.TransportClient
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll
using log4net;
using ProtoBuf;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Pilot.Transport
{
  public class TransportClient : IDisposable
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof (TransportClient));
    private readonly HttpClient _client;
    private readonly CookieContainer _cookie;
    private readonly HttpClientHandler _handler;
    private ICallbackReceiver _callbackReceiver;
    private volatile string _url;
    private volatile bool _active;
    private long _callbackCount;

    public bool Active => _active;

    public TransportClient()
    {
      ServicePointManager.DefaultConnectionLimit = 10;
      _cookie = new CookieContainer();
      _handler = new HttpClientHandler()
      {
        UseCookies = true,
        CookieContainer = _cookie,
        UseProxy = false
      };
      _client = new HttpClient((HttpMessageHandler) _handler, false);
    }

    public void Dispose()
    {
      _active = false;
      try
      {
        _client.CancelPendingRequests();
      }
      catch (Exception ex)
      {
      }
      _client.Dispose();
      _handler.Dispose();
    }

    public void Connect(string url)
    {
      _url = url;
      _active = true;
      ProcessResponse(_client.GetAsync(url + "/connect"));
    }

    public void SetProxy(IWebProxy proxy)
    {
      if (_handler.Proxy == proxy)
        return;
      _handler.UseProxy = proxy != null;
      ServicePointManager.Expect100Continue = !_handler.UseProxy;
      if (!_handler.UseProxy)
        return;
      _handler.Proxy = proxy;
    }

    public byte[] Call(byte[] data)
    {
      CheckActive();
      using (var byteArrayContent = new ByteArrayContent(data))
        return ProcessResponse(_client.PostAsync(_url + "/call", (HttpContent) byteArrayContent));
    }

    private byte[] ProcessResponse(Task<HttpResponseMessage> task)
    {
      var httpResponseMessage = (HttpResponseMessage) null;
      try
      {
        byte[] result;
        try
        {
          httpResponseMessage = task.Result;
          result = httpResponseMessage.Content.ReadAsByteArrayAsync().Result;
        }
        catch (AggregateException ex)
        {
          if (ex.InnerExceptions.Count == 1)
          {
            var innerException = ex.InnerExceptions[0];
            if (innerException is HttpRequestException)
              throw new TransportException(GetReadableMessage(innerException), innerException);
            if (innerException is WebException)
              throw new TransportException(innerException.Message, innerException);
            if (innerException is SocketException)
              throw new TransportException(innerException.Message, innerException);
            if (innerException is TaskCanceledException)
              throw new TransportException("Connection lost", innerException);
            throw innerException;
          }
          throw;
        }
        switch (httpResponseMessage.StatusCode)
        {
          case HttpStatusCode.OK:
            return result;
          case HttpStatusCode.BadRequest:
            _active = false;
            throw new TransportException("Connection lost");
          case HttpStatusCode.InternalServerError:
            throw ReadException(result);
          default:
            _active = false;
            throw new TransportException(httpResponseMessage.ReasonPhrase);
        }
      }
      finally
      {
        if (httpResponseMessage != null)
          httpResponseMessage.Dispose();
      }
    }

    private string GetReadableMessage(Exception exception)
    {
      var innerException = exception.InnerException;
      if (innerException == null)
        return exception.Message;
      if (innerException is WebException)
        throw new TransportException(innerException.Message, exception);
      if (innerException is SocketException)
        throw new TransportException(innerException.Message, exception);
      if (innerException is TaskCanceledException)
        throw new TransportException("Connection lost", exception);
      return exception.Message;
    }

    private Exception ReadException(byte[] data)
    {
      using (var memoryStream = new MemoryStream(data))
      {
        var protoExceptionInfo = (ProtoExceptionInfo) Serializer.Deserialize<ProtoExceptionInfo>((Stream) memoryStream);
        if (protoExceptionInfo == null)
          return (Exception) new ArgumentException("Can not deserialize exception from server");
        var exceptionType = protoExceptionInfo.ExceptionType;
        Exception instance;
        try
        {
          instance = Activator.CreateInstance(exceptionType, new object[1]
          {
            (object) protoExceptionInfo.ExceptionMessage
          }) as Exception;
        }
        catch (MissingMethodException ex)
        {
          instance = Activator.CreateInstance(exceptionType) as Exception;
        }
        return instance ?? (Exception) new ArgumentException("Can not deserialize exception from server");
      }
    }

    private void CheckActive()
    {
      if (!_active)
        throw new TransportException("Client connection is not active");
    }

    public void OpenCallback(ICallbackReceiver callbackReceiver)
    {
      CheckActive();
      if (callbackReceiver == null)
        throw new ArgumentNullException("callbackReceiver");
      _callbackReceiver = callbackReceiver;
      SendCallbackRequestAsync();
    }

    private void SendCallbackRequestAsync()
    {
      _client.GetAsync(_url + "/callback").ContinueWith(new Action<Task<HttpResponseMessage>>(ProcessCallback));
      Interlocked.Increment(ref _callbackCount);
    }

    private void ProcessCallback(Task<HttpResponseMessage> task)
    {
      Interlocked.Decrement(ref _callbackCount);
      if (_active && Interlocked.Read(ref _callbackCount) == 0L)
        SendCallbackRequestAsync();
      if (task.Status == TaskStatus.RanToCompletion)
      {
        using (var result1 = task.Result)
        {
          var result2 = result1.Content.ReadAsByteArrayAsync().Result;
          if (result1.StatusCode == HttpStatusCode.OK && result2.Length > 0)
            _callbackReceiver.Receive(result2);
          if (result1.StatusCode == HttpStatusCode.BadRequest)
          {
            _active = false;
            _callbackReceiver.Error();
          }
        }
      }
      if (task.Status != TaskStatus.Faulted)
        return;
      _active = false;
      _callbackReceiver.Error();
      if (task.Exception == null)
        return;
      task.Exception.Handle((Func<Exception, bool>) (ex => true));
    }

    public void Disconnect()
    {
      if (!_active)
        return;
      _active = false;
      try
      {
        ProcessResponse(_client.GetAsync(_url + "/disconnect"));
      }
      catch (Exception ex)
      {
        Logger.Error((object) string.Format("An error occured while disconnecting. URL: {0}", (object) _url), ex);
        throw;
      }
    }
  }
}
