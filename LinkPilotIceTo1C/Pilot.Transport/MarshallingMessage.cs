﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.MarshallingMessage
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using ProtoBuf;
using System.Collections.Generic;

namespace Pilot.Transport
{
  [ProtoContract]
  public class MarshallingMessage
  {
    [ProtoMember(1)]
    public string Interface { get; set; }

    [ProtoMember(2)]
    public string Method { get; set; }

    [ProtoMember(3)]
    public int ParamCount { get; set; }

    [ProtoMember(4)]
    public List<int> NullParamIndices { get; private set; }

    public MarshallingMessage()
    {
      NullParamIndices = new List<int>();
    }
  }
}
