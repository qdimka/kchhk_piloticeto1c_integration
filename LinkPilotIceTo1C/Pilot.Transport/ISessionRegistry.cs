﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.ISessionRegistry
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using System.Net;

namespace Pilot.Transport
{
  public interface ISessionRegistry
  {
    ISession NewSession(ISessionCallback callback, IMarshallingFactory marshallingFactory, IPEndPoint endPoint);

    void CloseSession(ISession session);

    void CloseCallback(ISession session, byte[][] unsentData);

    byte[][] RestoreCallback(ISession session);
  }
}
