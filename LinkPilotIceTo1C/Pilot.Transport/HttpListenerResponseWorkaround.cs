﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.HttpListenerResponseWorkaround
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using log4net;
using System;
using System.ComponentModel;
using System.Net;

namespace Pilot.Transport
{
  public static class HttpListenerResponseWorkaround
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof (HttpListenerResponseWorkaround));

    public static void SendAndClose(this HttpListenerResponse response, byte[] data, bool willBlock)
    {
      if (willBlock)
        response.Close(data, true);
      else
        response.OutputStream.BeginWrite(data, 0, data.Length, new AsyncCallback(NonBlockingCloseCallback), (object) response);
    }

    private static void NonBlockingCloseCallback(IAsyncResult asyncResult)
    {
      var asyncState = (HttpListenerResponse) asyncResult.AsyncState;
      try
      {
        asyncState.OutputStream.EndWrite(asyncResult);
      }
      catch (Win32Exception ex)
      {
      }
      try
      {
        asyncState.Close();
      }
      catch (InvalidOperationException ex)
      {
        Logger.Error((object) ex.Message, (Exception) ex);
      }
    }
  }
}
