﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.TransportServer
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace Pilot.Transport
{
  public class TransportServer : IDisposable
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof (TransportServer));
    private readonly HttpListener _listener = new HttpListener();
    private readonly ConcurrentDictionary<Guid, Session> _sessions = new ConcurrentDictionary<Guid, Session>();
    private readonly ISessionRegistry _registry;
    private readonly int _callbackTimeout;
    private readonly int _sessionTimeout;
    private volatile bool _listening;

    public TransportServer(ISessionRegistry registry, int callbackTimeout, int sessionTimeout)
    {
      _listener.IgnoreWriteExceptions = true;
      _registry = registry;
      _callbackTimeout = callbackTimeout;
      _sessionTimeout = sessionTimeout;
    }

    public void Dispose()
    {
      Stop();
    }

    public void Start(string url)
    {
      _listener.Prefixes.Add(url + "/");
      _listening = true;
      _listener.Start();
      _listener.BeginGetContext(new AsyncCallback(ProcessGetContextAsync), (object) _listener);
    }

    public void Stop()
    {
      if (!_listening)
        return;
      _listening = false;
      CloseSessions();
      _listener.Close();
    }

    private void CloseSessions()
    {
      for (var values = _sessions.Values; values.Count > 0; values = _sessions.Values)
        Parallel.ForEach<Session>((IEnumerable<Session>) values, (Action<Session>) (session => CloseSession(session)));
    }

    private void ProcessGetContextAsync(IAsyncResult result)
    {
      HttpListenerContext context1;
      try
      {
        var asyncState = (HttpListener) result.AsyncState;
        if (_listening && asyncState.IsListening)
          asyncState.BeginGetContext(new AsyncCallback(ProcessGetContextAsync), (object) asyncState);
        context1 = asyncState.EndGetContext(result);
      }
      catch
      {
        return;
      }
      var context2 = new Context(context1);
      try
      {
        ProcessRequest(context2);
      }
      catch (HttpListenerException ex)
      {
        Logger.Error((object) "We can do nothing here.", (Exception) ex);
      }
      catch (Exception ex1)
      {
        Logger.Info((object) "ProcessRequest error", ex1);
        try
        {
          context2.Send(ex1);
        }
        catch (Exception ex2)
        {
          Logger.Error((object) "ProcessRequest Send error", ex2);
        }
      }
    }

    private Session GetSession(Context context)
    {
      var cookie = context.GetCookie("SID");
      if (string.IsNullOrEmpty(cookie))
        return (Session) null;
      Guid result;
      if (!Guid.TryParse(cookie, out result))
        return (Session) null;
      Session session;
      if (!_sessions.TryGetValue(result, out session))
        return (Session) null;
      return session;
    }

    private void ProcessRequest(Context context)
    {
      switch (context.Method)
      {
        case "/connect":
          DoConnect(context);
          break;
        case "/call":
          DoCall(context);
          break;
        case "/callback":
          DoCallback(context);
          break;
        case "/disconnect":
          DoDisconnect(context);
          break;
        case "/":
          DoPing(context);
          break;
        case "/web/connect":
          DoWebConnect(context);
          break;
        case "/web/call":
          DoCall(context);
          break;
        case "/web/file":
          DoFile(context);
          break;
        default:
          throw new ArgumentException(string.Format("Unknown request [{0}]", (object) context.Method));
      }
    }

    private void DoPing(Context context)
    {
      context.Send(string.Format("Pilot-Server_v{0}", (object) Assembly.GetExecutingAssembly().GetName().Version));
    }

    private void DoConnect(Context context)
    {
      var session = new Session(_registry, _callbackTimeout, _sessionTimeout, new Session.OnSessionTimeout(CloseSession), (IMarshallingFactory) new MarshallingFactory(), context.GetRemoteEndPoint());
      _sessions.TryAdd(session.Id, session);
      context.SetCookie("SID", session.Id.ToString(), "/");
      context.Send();
    }

    private void DoCall(Context context)
    {
      var session = GetSession(context);
      if (session == null)
        context.BadSession();
      else
        session.ProcessCallRequest(context);
    }

    private void DoCallback(Context context)
    {
      var session = GetSession(context);
      if (session == null)
        context.BadSession();
      else
        session.ProcessCallbackRequest(context);
    }

    private void DoDisconnect(Context context)
    {
      var session = GetSession(context);
      if (session != null)
        CloseSession(session);
      context.Send();
    }

    private void CloseSession(Session session)
    {
      session.Close(_registry);
      Session session1;
      _sessions.TryRemove(session.Id, out session1);
    }

    private void DoWebConnect(Context context)
    {
      var session = new Session(_registry, _callbackTimeout, _sessionTimeout, new Session.OnSessionTimeout(CloseSession), (IMarshallingFactory) new JsonMarshallingFactory(), context.GetRemoteEndPoint());
      _sessions.TryAdd(session.Id, session);
      context.SetCookie("SID", session.Id.ToString(), "/", "");
      context.Send();
    }

    private void DoFile(Context context)
    {
      var session = GetSession(context);
      if (session == null)
        context.BadSession();
      else
        session.ProcessGetRequest(context);
    }
  }
}
