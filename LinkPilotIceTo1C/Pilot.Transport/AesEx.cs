﻿// Decompiled with JetBrains decompiler
// Type: Pilot.Transport.AesEx
// Assembly: Pilot.Transport, Version=17.0.9.15643, Culture=neutral, PublicKeyToken=null
// MVID: 843B5E05-CEBF-4DD2-A45B-544B2458740F
// Assembly location: C:\Program Files\ASCON\Pilot-ICE Enterprise\Pilot.Transport.dll

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Pilot.Transport
{
  internal static class AesEx
  {
    private static readonly byte[] Password = Encoding.Unicode.GetBytes("123456");
    private static readonly byte[] Salt = Encoding.Unicode.GetBytes("{5AF2954F-02FC-445A-BC27-976DD11A6258}");
    private const int Iterations = 1000;

    public static string EncryptAes(this string plainText)
    {
      byte[] array;
      using (var aes = Aes.Create())
      {
        using (var deriveBytes = (DeriveBytes) new Rfc2898DeriveBytes(Password, Salt, 1000))
        {
          aes.Key = deriveBytes.GetBytes(aes.KeySize >> 3);
          aes.IV = deriveBytes.GetBytes(aes.BlockSize >> 3);
          var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
          using (var memoryStream = new MemoryStream())
          {
            using (var cryptoStream = new CryptoStream((Stream) memoryStream, encryptor, CryptoStreamMode.Write))
            {
              using (var streamWriter = new StreamWriter((Stream) cryptoStream))
                streamWriter.Write(plainText);
              array = memoryStream.ToArray();
            }
          }
        }
      }
      return Convert.ToBase64String(array);
    }
  }
}
