﻿namespace Pilot.Core
{
    public static class Constants
    {
        public const string ProjectTitleAttributesDelimiter = " - ";
        public const int MaxProjectItemFolderNameLength = 40;
        public const string TruncatedNameMark = "~";
        public const string AnnotationsDefinition = "Annotation";
        public const string AnnotationChatMessage = "Note_Chat_Message";
        public const string DigitalSignature = "PilotDigitalSignature";
        public const string ThumbnailFileNamePostfix = "PilotThumbnail";
        public const string TextLabelsDefinition = "PilotTextLabels";
        public const string BarcodeDefinition = "PilotBarcode";        
        public static readonly char[] Separators = { ' ', '.', ',', '"', '\'', '!', '?', ':', ';' };
        public const int MaxItemsLoadPerPage = 250;
    }
}
