﻿namespace Pilot.Core.Numerators
{
    public class UnknownProvider : INumeratorKeywordProvider
    {
        public object GetValue(DObject obj, string keyword)
        {
            return "***";
        }
    }
}
