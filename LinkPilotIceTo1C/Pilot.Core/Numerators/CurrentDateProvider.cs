﻿using System;

namespace Pilot.Core.Numerators
{
    public class CurrentDateProvider : INumeratorKeywordProvider
    {
        public const string CurrentDateKeyword = "CurrentDate";

        public object GetValue(DObject obj, string keyword)
        {
            if (keyword != CurrentDateKeyword)
                return null;

            return DateTime.Now;
        }
    }
}
