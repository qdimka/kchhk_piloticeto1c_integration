﻿using System;
using System.Security.Cryptography;

namespace Pilot.Core
{
    public static class PasswordEx
    {
        private const int SaltByteSize = 24;
        private const int HashByteSize = 24;
        private const int Pbkdf2Iterations = 1000;

        private const int IterationIndex = 0;
        private const int SaltIndex = 1;
        private const int Pbkdf2Index = 2;

        private const char Delimiter = ':' ;

        public static string CreateHash(this string password)
        {
            var salt = RandomSalt();
            var hash = Pbkdf2(password, salt, Pbkdf2Iterations, HashByteSize);

            return string.Format("{0}{1}{2}{3}{4}", Pbkdf2Iterations, Delimiter, Convert.ToBase64String(salt), Delimiter, Convert.ToBase64String(hash)); 
        }

        private static byte[] RandomSalt()
        {
            var salt = new byte[SaltByteSize];
            using (var csprng = RandomNumberGenerator.Create())
                csprng.GetBytes(salt);
            return salt;
        }

        private static byte[] Pbkdf2(string password, byte[] salt, int iterations, int outputBytes)
        {
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations))
                return pbkdf2.GetBytes(outputBytes);
        }

        public static bool ValidatePassword(this string password, string correctHash)
        {
            try
            {
                var split = correctHash.Split(Delimiter);
                var iterations = Int32.Parse(split[IterationIndex]);
                var salt = Convert.FromBase64String(split[SaltIndex]);
                var hash = Convert.FromBase64String(split[Pbkdf2Index]);

                var testHash = Pbkdf2(password, salt, iterations, hash.Length);
                return SlowEquals(hash, testHash);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Compares two byte arrays in length-constant time. This comparison
        /// method is used so that password hashes cannot be extracted from
        /// on-line systems using a timing attack and then attacked off-line.
        /// </summary>
        /// <param name="a">The first byte array.</param>
        /// <param name="b">The second byte array.</param>
        /// <returns>True if both byte arrays are equal. False otherwise.</returns>
        private static bool SlowEquals(byte[] a, byte[] b)
        {
            var diff = (uint)a.Length ^ (uint)b.Length;
            for (var i = 0; i < a.Length && i < b.Length; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }
    }
}