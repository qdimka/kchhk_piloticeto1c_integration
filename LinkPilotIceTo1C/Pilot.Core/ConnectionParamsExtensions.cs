﻿using System;

namespace Pilot.Core
{
    public static class ConnectionParamsExtensions
    {
        public static Uri Url(this ConnectionParams connectParams)
        {
            var normalizedServerUri = ConnectionValidator.NormalizeUri(connectParams.Server);
            return new Uri(normalizedServerUri.ToLower());
        }

        public static bool IsValid(this ConnectionParams connectParams)
        {
            if (connectParams == null)
                return false;

            var baseConfigOk = !string.IsNullOrEmpty(connectParams.Server) && !String.IsNullOrEmpty(connectParams.UserName);
            var proxyOk = (!String.IsNullOrEmpty(connectParams.Proxy.Url) && connectParams.Proxy.Port != 0);

            //не использовать прокси
            if (!connectParams.Proxy.IsRequired)
                return baseConfigOk;

            //использовать прокси без авторизации
            if (connectParams.Proxy.IsRequired && !connectParams.Proxy.IsAuthRequired)
                return baseConfigOk && proxyOk;

            //использовать прокси с авторизацией
            if (connectParams.Proxy.IsRequired && connectParams.Proxy.IsAuthRequired)
                return baseConfigOk && !String.IsNullOrEmpty(connectParams.Proxy.UserName);

            //По умолчанию
            return false;
        }

        public static string ServerName(this ConnectionParams connectParams)
        {
            return string.IsNullOrEmpty(connectParams.Server) 
                ? string.Empty 
                : Url(connectParams).Host.ToLower();
        }

        public static string Db(this ConnectionParams connectParams)
        {
            return string.IsNullOrEmpty(connectParams.Server) 
                ? string.Empty 
                : Uri.UnescapeDataString(Url(connectParams).AbsolutePath.TrimStart('/'));
        }
    }
}
