﻿using System.Collections.Generic;

namespace Pilot.Core
{
    public static class SystemAttributes
    {
        public const string DeleteDate = "DeleteDate 9349ED7C-C2D7-4B8B-852A-83140F158611";
        public const string DeleteInitiatorPerson = "DeleteInitiatorPerson BE56ECD1-F4C5-40E2-A83C-274EAE4D02A9";
        public const string DeleteInitiatorPosition = "DeleteInitiatorPosition 35E355AC-97B3-40FC-9636-1648402040D4";
        public const string SearchCriteria = "SearchCriteria 52F6E73A-D736-49CD-8807-5AD955506A37";
        public const string SmartFolderTitle = "SmartFolderTitle 3AE3FFC8-A776-4E61-87D0-FFD8B50CBBA8";
        public const string SearchContextObjectId = "SearchContextObjectId 257E6DB2-F3A3-4231-83D4-DB57C3FF059E";

        public const string IsHidden = "IsHidden FDF5475C-93FA-41F3-8243-F1810854DEBD";
        public const string ProjectItemName = "Title 4C281306-E329-423A-AF45-7B39EC30273F";

        public const string TaskExecutorPosition = "Executor 7620B1D4-B476-4169-8401-6A0C3D3A22F3";
        public const string TaskExecutorRole = "ExecutorRole B9628414-E52A-483A-98B0-58C11ED1756B";
        public const string TaskState = "TaskState B65D6C5B-7D8E-4055-852F-D1AAB060CD22";
        public const string TaskDeadlineDate = "TaskDeadlineDate DE08B9D6-59AF-42BE-9DA9-2B327D439A05";
        public const string TaskWithValidationPhase = "TaskWithValidationPhase 7A626A12-E416-4F1C-BC8B-70F178DFD264";
        public const string TaskInitiatorPosition = "Initiator CEA10431-5A8C-4F2A-965B-E780A58142B7";
        public const string TaskTitle = "Title 578C65E3-A95E-44A5-942A-D3FA04956B82";
        public const string TaskDescription = "Description 6589B3AC-CE5B-43B4-902B-F88A1F396E25";
        public const string TaskStageOrder = "Order F06F94A0-2C3B-4C0C-B900-4AE422F2FAC7";
        public const string TaskDateOfAssignment = "DateOfAssignment B8A06EEF-8572-4875-B02D-BA3D37C31BDD";
        public const string TaskMessageTextAttributeName = "Task_message_text 05DD1EC5-6BC6-4901-B008-B1B648EC6ACD";
        public const string TaskAgreement = "TaskAgreement B4B97159-8CED-48F8-BED2-521E632F7A21";
        public const string TaskByCopyTo = "TaskByCopyTo 6B50090E-67DD-421E-8169-0BE25EB8F2E7";
        public const string TaskIsVersion = "TaskIsVersion_EECBDB6A-E16B-4BE6-B901-AF895301C8BD";
        public const string TaskDateOfCompletion = "DateOfCompletion_C8FEC8EC-B9DD-4231-A46F-DCD2D0A3D082";

        public const string ExtensionFolderName = "Extension_folder_name EE4DE4B7-FFB7-455E-8E15-C185CFDB34FA";
        public const string ExtensionName = "Extension_name 53522556-0749-46DE-913E-EDA195AD1299";
        public const string ExtensionAdditional = "Extension_additional 9A428222-2031-4F5E-A367-C5BAA18DFCA5";

        public const string ShortcutObjectId = "Shortcut_object_id_CF11FD82-3D56-4DBC-B7F8-DF91CA1F9885";
        public const string ReportFolderName = "Report_folder_name_BDBDEDD1-BFCB-4E2C-BE44-3E4BEBBB58F9";
        public const string ReportName = "Report_name_D745D627-E7CD-4E3A-B30E-F9EDC1A09D77";

        public const string CreationTime = "creation_time";
        
        public static IEnumerable<string> GetTextSystemAttributes()
        {
            yield return ProjectItemName;
            yield return TaskTitle;
            yield return TaskDescription;
            yield return TaskMessageTextAttributeName;
        }

        public static IEnumerable<string> GetAllSystemAttributes()
        {
            yield return DeleteDate;
            yield return DeleteInitiatorPerson;
            yield return DeleteInitiatorPosition;
            yield return SearchCriteria;
            yield return SmartFolderTitle;
            yield return SearchContextObjectId;

            yield return IsHidden;
            yield return ProjectItemName;

            yield return TaskExecutorPosition;
            yield return TaskExecutorRole;
            yield return TaskState;
            yield return TaskDeadlineDate;
            yield return TaskWithValidationPhase;
            yield return TaskInitiatorPosition;
            yield return TaskTitle;
            yield return TaskDescription;
            yield return TaskStageOrder;
            yield return TaskDateOfAssignment;
            yield return TaskMessageTextAttributeName;
            yield return TaskAgreement;
            yield return TaskByCopyTo;
            yield return TaskIsVersion;
            yield return TaskDateOfCompletion;

            yield return ExtensionName;
            yield return ExtensionFolderName;
            yield return ExtensionAdditional;

            yield return ReportFolderName;
            yield return ReportName;
        }
    }

    public static class SystemTypes
    {
        public const string ProjectFolder = "Project_folder";
        public const string ProjectFile = "File";
        public const string Task = "Task";
        public const string TaskStage = "Task_Stage";
        public const string TaskWorkflow = "Task_Workflow";
        public const string TaskFolder = "Task_Folder";
        public const string TaskChat = "Chat_type";
        public const string TaskChatMessage = "Chat_message_type";
        public const string Extension = "Extension";
        public const string ExtensionFolder = "Extension_folder";
	    public const string Shortcut = "Shortcut_E67517F1-93F5-4756-B651-133B816D43C8";
        public const string Report = "Report_6088AF81-061E-456E-9225-CF65B7B25368";
        public const string ReportFolder = "Report_Folder_F2CC6F1D-70E1-4E9B-B32F-BEB3E991318F";
        public const string SmartFolder = "Smart_folder_type";

        public static IEnumerable<string> All()
        {
            yield return ProjectFolder;
            yield return ProjectFile;
            yield return Task;
            yield return TaskStage;
            yield return TaskWorkflow;
            yield return TaskFolder;
            yield return TaskChat;
            yield return Extension;
            yield return ExtensionFolder;
            yield return Shortcut;
            yield return Report;
            yield return ReportFolder;
            yield return SmartFolder;
        }
    }
}
