﻿using LinkPilotIceTo1C.ExternalClient.Service;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Windows.Forms;

namespace AddIn
{
    [Guid("AB634001-F13D-11D0-A459-004095E1DAEA")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IInitDone
    {
        /// <summary>
        /// Инициализация компонента
        /// </summary>
        /// <param name="connection">reference to IDispatch</param>
        void Init([MarshalAs(UnmanagedType.IDispatch)] object connection);

        /// <summary>
        /// Вызывается перед уничтожением компонента
        /// </summary>
        void Done();

        /// <summary>
        /// Возвращается инициализационная информация
        /// </summary>
        /// <param name="info">Component information</param>
        void GetInfo([MarshalAs(UnmanagedType.SafeArray, SafeArraySubType = VarEnum.VT_VARIANT)] ref object[] aInfo);
    }

    [Guid("AB634004-F13D-11D0-A459-004095E1DAEA")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IAsyncEvent
    {
        /// <summary>
        /// Установка глубины буфера событий
        /// </summary>
        /// <param name="depth">Глубина буфера</param>
        /// <remarks>
        /// <prototype>
        /// HRESULT SetEventBufferDepth(long lDepth);
        /// </prototype>
        /// </remarks>
        void SetEventBufferDepth(Int32 depth);

        /// <summary>
        /// Получение глубины буфера событий
        /// </summary>
        /// <param name="depth">Глубина буфера</param>
        /// <remarks>
        /// <prototype>
        /// HRESULT GetEventBufferDepth(long *plDepth);
        /// </prototype>
        /// </remarks>
        void GetEventBufferDepth(ref Int32 depth);

        /// <summary>
        /// Посылка события
        /// </summary>
        /// <param name="source">Источник</param>
        /// <param name="message">Сообщение</param>
        /// <param name="data">Данные</param>
        /// <remarks>
        /// <prototype>
        /// HRESULT GetEventBufferDepth(long *plDepth);
        /// </prototype>
        /// </remarks>
        void ExternalEvent(
            [MarshalAs(UnmanagedType.BStr)] String source,
            [MarshalAs(UnmanagedType.BStr)] String message,
            [MarshalAs(UnmanagedType.BStr)] String data
        );

        /// <summary>
        /// Очистка буфера событий
        /// </summary>
        /// <remarks>
        /// <prototype>
        /// </prototype>
        /// </remarks>
        void CleanBuffer();
    }

    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface ITest
    {
        void SelectObject(String inLink);
        void Print(String typeEvent, String dataEvent);
    }

    [ClassInterface(ClassInterfaceType.AutoDual)]
  //  [ProgId("AddIn.LoodsmanPGSClass")]
    public class LoodsmanPgsClass : IInitDone, ITest
    {
        private static IAsyncEvent _asyncEvent;

        //private IStatusLine statusLine = null;
        [DllImport("ole32.dll")]
        private static extern int GetRunningObjectTable(uint reserved, out IRunningObjectTable pprot);

        [DllImport("ole32.dll", EntryPoint = "CreateItemMoniker")]
        public static extern int CreateItemMoniker(byte[] lpszDelim, byte[] lpszItem, out IMoniker ppmk);

        public LoodsmanPgsClass()
        {
            
        }

        /// <summary>
        /// Инициализация компонента
        /// </summary>
        /// <param name="connection">reference to IDispatch</param>
        public void Init([MarshalAs(UnmanagedType.IDispatch)] Object pConnection)
        {
            try
            {
                Registr();
                _asyncEvent = (IAsyncEvent)pConnection;
            }
            catch
            {
                throw new COMException(@"Unknown object context");
            }
        }
        
        /// <summary>
        /// Возвращается информация о компоненте
        /// </summary>
        /// <param name="info">Component information</param>

        public void GetInfo([MarshalAs(UnmanagedType.SafeArray, SafeArraySubType = VarEnum.VT_VARIANT)] ref Object[] pInfo)
        {
            pInfo[0] = @"2000";
        }
        
        public void Done()
        {
            _asyncEvent = null;
        }

        private object GetApiInstance()
        {
            IRunningObjectTable tmp;
            GetRunningObjectTable(0, out tmp);
            IMoniker moniker = null;
            CreateItemMoniker((new UnicodeEncoding()).GetBytes("!"),
                (new UnicodeEncoding()).GetBytes("AddIn.LoodsmanPGSClass"), out moniker);

            object tmpObject = null;
            tmp.GetObject(moniker, out tmpObject);

            return tmpObject;
        }

        public void Registr()
        {
            if (GetApiInstance() == null)
            {
                IRunningObjectTable tmp;
                GetRunningObjectTable(0, out tmp);
                IMoniker moniker = null;
                CreateItemMoniker((new UnicodeEncoding()).GetBytes("!"),
                    (new UnicodeEncoding()).GetBytes("AddIn.LoodsmanPGSClass"), out moniker);
                tmp.Register(0, this, moniker);
            }
        }

        //[DllImport("LoodsmanPGSDLL.dll")]
        //public static extern void ResPath([MarshalAs(UnmanagedType.BStr)] out string s);

        public void SelectObject(String inLink)
        {
            var service = ExternalService.Service;

            service.StringAvalaible += (s, e) => {
                Print("LoodsmanLink",(e as StringAvalaibleEventArgs).PilotUrl);
            };

            service.Start(inLink);
            //Print("LoodsmanLink", ExternalService.Start(inLink));
        }

        public void Print(String typeEvent, String dataEvent)
        {
            if (_asyncEvent == null)
                MessageBox.Show("Компонента работает ненормально");
            else
            {
                var oParms = new Object[] { "AddIn.LoodsmanPGS", typeEvent, dataEvent };
                _asyncEvent.GetType().InvokeMember("ExternalEvent", BindingFlags.InvokeMethod, null, _asyncEvent, oParms);
                //asyncEvent.ExternalEvent("AddIn", "Type", link);
            }
        }
    }

}
