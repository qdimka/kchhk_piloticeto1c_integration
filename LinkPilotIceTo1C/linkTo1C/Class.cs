﻿using System;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Text;
using System.Runtime.InteropServices.ComTypes;
using Proxy.Net;
using Ascon.AppPlugin;

namespace ClassLibrary1
{
    public class Main : NonVisualPlugin, IPlugin, IPluginCoolCommands
    {
        private IPluginAppProvider _main;
        private object _comObject;

        public int GetVersion()
        {
            return PluginConsts.PluginVersion;
        }

        public string GetLastError()
        {
            throw new NotImplementedException();
        }

        [DllImport("ole32.dll")]
        private static extern int GetRunningObjectTable(uint reserved, out IRunningObjectTable pprot);

        [DllImport("ole32.dll", EntryPoint = "CreateItemMoniker")]
        public static extern int CreateItemMoniker(byte[] lpszDelim, byte[] lpszItem, out IMoniker ppmk);

        private object GetApiInstance()
        {
            IRunningObjectTable tmp;
            GetRunningObjectTable(0, out tmp);
            IMoniker moniker = null;
            CreateItemMoniker((new UnicodeEncoding()).GetBytes("!"),
                (new UnicodeEncoding()).GetBytes("AddIn.LoodsmanPGSClass"), out moniker);

            object tmpObject = null;
            tmp.GetObject(moniker, out tmpObject);

            return tmpObject;
        }

        public void Init(IPluginAppProvider appProvider)
        {
            _main = appProvider;

            _comObject = GetApiInstance();

            //object[] oParms = new Object[] { 1 };
            //short sum = (short)objType.InvokeMember("Add",
            //               BindingFlags.InvokeMethod, null, comObject, oParms);

            /*
            IPluginAppWorkFlow _WF;
            _WF = appProvider.GetAppWorkFlow();

            IPluginAppMain _Main;
            _Main = appProvider.GetAppMain();

            IPluginAppCache _Cache;
            _Cache = appProvider.GetAppCache();

            int idNewRoute;
            _WF.CreateTask("Задание создано в C# 2", "Текст задания", 0, 55, 73, DateTime.Now, true, 
                    0,
                    //bpkTask, 
                    out idNewRoute);
            _WF.StartTask(idNewRoute, 55, 73, "Уведомление по Задание создано в C#", DateTime.Now);

            System.Windows.Forms.MessageBox.Show(_Cache.GetUserFullName());


            IPluginDataSet ds1;
            _WF.GetUserList(out ds1);
            ds1.First();
            System.Windows.Forms.MessageBox.Show(ds1.AsString("_NAME"));
            ds1.Next();
            System.Windows.Forms.MessageBox.Show(ds1.AsString("_NAME"));
            ds1.Last();
            System.Windows.Forms.MessageBox.Show(ds1.AsString("_NAME"));
            // используйте @ для задания строк вида "DOMAIN\user1"
            if (ds1.Locate("_NAME", @"zavrazina"))
            {
                System.Windows.Forms.MessageBox.Show("if Locate " + ds1.AsString("_FULLNAME"));
            }
            int rCount = ds1.RecordCount;
            System.Windows.Forms.MessageBox.Show(Convert.ToString(rCount));
            System.Windows.Forms.MessageBox.Show(ds1.Field(1));
            ds1 = null;


            _WF = null;
            _Main = null;
            _Cache = null;
*/
        }

        public void InitCoolCommands(IPluginCoolCommandsPanel accPanel)
        {
            if (_comObject == null)
                _comObject = GetApiInstance();
            if (_comObject != null)
                accPanel.AddCommand("Добавить файл в 1С:ТОиР", "1С:ТОиР", "", 0);
        }

        public void CallCoolCommand(int aIndex)
        {
            if (aIndex == 0)
            {
                if (_comObject == null)
                    _comObject = GetApiInstance();
                if (_comObject != null)
                {
                    var linkValue = String.Format("{0}::pgsp://{1}/{2}", _main.GetSelectedObjects().Version(0).Title(), _main.GetAppCache().GetDbName(), _main.GetSelectedObjects().Version(0).IdVersion);
                    var oParms = new Object[] { "LoodsmanLink", linkValue };
                    try
                    {
                        _comObject.GetType().InvokeMember("Print", BindingFlags.InvokeMethod, null, _comObject, oParms);
                    }
                    catch
                    {
                        _comObject = null;
                        _comObject = GetApiInstance();
                        try
                        {
                            _comObject.GetType().InvokeMember("Print", BindingFlags.InvokeMethod, null, _comObject, oParms);
                        }
                        catch
                        {
                            _comObject = null;
                            System.Windows.Forms.MessageBox.Show("Компонента связи с 1С не найдена", "Плагин связи", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        }
                    }
                    //String linkFileValue = String.Empty;
                    //_Main.GetAppMain().Client_GetFilePath(_Main.GetSelectedObjects().Version(0).IdVersion, "Файл.xps", out linkFileValue);
                    //oParms = new Object[] { "LoodsmanFileLink", linkFileValue };
                    //comObject.GetType().InvokeMember("Print", BindingFlags.InvokeMethod, null, comObject, oParms);
                }
            }
            /*
             if (aIndex == 1)
             {
                 int newIdCheckOut;
                 _Main.CheckOut("", "", "", 0, out newIdCheckOut);
             }
 */

            // throw new NotImplementedException();
        }
    }
}
