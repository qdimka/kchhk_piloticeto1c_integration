﻿namespace Ascon.AppPlugin
{
    public interface IAppProvider
    {
        string GetAppTitle();
        int GetAppHandle();

        IAppMain AppMain { get; }
        IAppWorkFlow AppWorkFlow { get; }
        IAppCache AppCache { get; }
        IAppConfigurator AppConfigurator { get; }
        
        IPluginPdmVersions GetSelectedObjects();
        void RefreshVersion(int idVersion);
        void SelectObject(int idVersion);
        void SelectTask(int idProcess, TasksFilterExp tasksFilter);
        void SelectMail(int idLetter, MailFilterExp filter);

        IPluginTasks GetSelectedTasks();
        ModalResultExp GetAttachFromDlg(out IPluginPdmVersions versions);
        ModalResultExp GetUsersFromDlg(bool aDateVisible, bool aSignRoleVisible, bool aMultiselect, bool aCanIAm,
                IPluginUsers aSelectedUsers, out IPluginUsers user);
        void RefreshTasks(int aSelectedRouteId);
    }
}
