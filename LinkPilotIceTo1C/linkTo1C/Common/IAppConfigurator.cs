﻿namespace Ascon.AppPlugin
{
    public interface IAppConfigurator
    {
        IPluginDataSet GetSortCase(string stProfile, string stType, string stLink, int inIndex);
        IPluginDataSet GetSortTypes(int inIdCase);
        IPluginDataSet GetSortTree(int inIdCase);
        IPluginDataSet GetSortTable(int inIdCase);
    }
}
