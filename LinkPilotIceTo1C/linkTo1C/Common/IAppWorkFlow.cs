﻿using System;

namespace Ascon.AppPlugin
{
	public interface IAppWorkFlow
	{
		int CreateTask(string subj, string text, int idVersion, int idRole, int idUser, DateTime datePlan, int criticalTerm, bool checkResult, TbProcessKindExp kind);
		void StartTask(int idRoute, int idRole, int idUser, string subj, DateTime datePlan);
		int CreateTaskByDialog();
		IPluginDataSet GetIncomingCorrespondence(int inRetMode);
		IPluginDataSet GetOutcomingCorrespondence(int inRetMode);
		IPluginDataSet GetCorrespondenceList(int inIdRoute);
		void DelLetter(int inIdLetter);
		IPluginDataSet GetUserList();
		IPluginDataSet GetRoleTree(int inMode);
		IPluginDataSet GetProcessListByObject(string stDbName, int inPdmId);
		IPluginDataSet GetProcessListByObjectEx(int inPdmId, string stDbName);
		IPluginDataSet GetInitialProcessList();
		IPluginDataSet GetInfoAboutCurrentUser();
		IPluginDataSet GetInfoAboutRoute(int inIdRoute);
		IPluginDataSet GetRoleListForUser(int inIdUser);
		IPluginDataSet GetTaskListForUser(int inIdUser, int inUserRoleMask, int inProcessStateMask, int inStageStateMode);
		int CreateNewProcess(string stDbName, int inIdPdmObject, string stRouteName);
		void DeleteProcess(int inIdRoute);
		void InsUpdStageIntoBp(string stTask, int inCriticalTerm, DateTime dtDatePlan, int inCanControl, int inActive, int inIdRole, int inIdUser,
				int inIdBProc, int inCanTransform, ref int inIdStage, ref int inTimeConstraint, ref int inFromBpStart);
		void AddRouteItemsSeq(int inIdFirst, int inIdSecond, int inType);
		void AddRouteItemsRetSeq(int inIdFirst, int inIdSecond);
		IPluginDataSet GetRoleListIntoProcess(int inIdRoute);
		void SetStateOfProcess(int inIdRoute, int inStateBp);
		void InsUpdUser(string stUserName, string stDescr, string stEMail, int inIsAdmin, ref int vaIdUser);
		void SetTaskState(int inIdRoute, int inIdOperation, int inStateTask, string stMessageBody);
		void SetItemState(int inIdItem, int inState, int inAddSignature);
		void SetItemStateInternal(int inIdItem, int inState);
		void AddBpDocument(int inIdRoute, int inIdDoc, int inIdPar);
		void DelBpDocument(int inIdRoute, int inIdDoc);
		IPluginDataSet GetFirstItems(int inIdRoute);
		IPluginDataSet GetNextItems(int inIdItem);
		IPluginDataSet rep_ExecutedJobs(int inIdParent);
		IPluginDataSet GetJobsListIntoProcess(int inIdRoute);
		IPluginDataSet GetAttachDocumentListIntoProcess(int inIdRoute);
		void StartNextStage(int inIdRoute, int inIdItem, int inDirect);
		void RenameProcess(int inIdRoute, string stNewName);
		void SetStageExecutor(int inIdStage, int inIdUser, int inIdRole);
		void SetStageTerm(int inIdStage, int inTimeLimit, DateTime vaPlanEnd, int inTimeConstraint, int inFromBpStart);
		void SetStageTask(int inIdStage, string stTask);
		void SetBP_PDMObject(int inIdRoute, int inIdPdmObject);
		IPluginDataSet GetTaskListForObject(int inIdVersion, int inProcessStateMask, int inStageStateMode);
		IPluginDataSet GetInfoAboutStage(int inIdStage);

		void GetVariable(int inIdRoute, string stName, out int res);
		void GetVariable(int inIdRoute, string stName, out string res);
		void GetVariable(int inIdRoute, string stName, out DateTime res);
		void GetVariable(int inIdRoute, string stName, out double res);

		void SetVariable(int inIdRoute, string stName, int value, bool readOnlyForChild);
		void SetVariable(int inIdRoute, string stName, string value, bool readOnlyForChild);
		void SetVariable(int inIdRoute, string stName, DateTime value, bool readOnlyForChild);
		void SetVariable(int inIdRoute, string stName, double value, bool readOnlyForChild);

		int SendLetterByBpToOneUser(int inIdRoute, int inIdAuRole, int inIdRecRole, int inIdRecUser, string stSubj, string stBody);
		int SendLetterByBpToUsers(int inIdRoute, int inIdAuRole, int inIdRecRole, int inIdRecUser, string stSubj, string stBody);

		IPluginDataSet GetTypicalProcessList(int inMode);
		int CreateNewProcessBy(string stDbName, int inIdPdmObject, string stRouteName, int inIdBaseRoute);
		int InsUpdOpInstance(int inIdOperDescr, int inIsWait, int inIdProc, int inIdOperInst);
	}
}