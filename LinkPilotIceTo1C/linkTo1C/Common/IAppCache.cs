﻿namespace Ascon.AppPlugin
{
	public interface IAppCache
	{
		string GetDbName();
		string GetUserName();
		string GetUserFullName();

		IPluginDataSet GetTypes();
		IPluginDataSet GetStates();
		IPluginDataSet GetDbProperties();
		IPluginDataSet GetLinks();
		IPluginDataSet GetAttributeList();
		IPluginDataSet GetUserList();
		IPluginDataSet GetInfoAboutType(string typeName, int inMode);
		
		/// <summary>
		/// Аналог стандартной GetLinkAttrForTypes.
		/// </summary>
		/// <returns>Возвращает возможные атрибуты связей для связки типов.</returns>
		IPluginDataSet GetLinkAttrForTypes(string parentType, string typeObj, string linkType);
		IPluginDataSet GetInfoAboutCard(string cardName, int inMode);
		IPluginDataSet GetInfoAboutAttribute(string attrName, int inMode);
		IPluginDataSet GetInfoAboutCurrentBase(int inMode);
		IPluginDataSet GetAutoOperationList();

		string GetTypeNameById(int id);
		string GetStateNameById(int id);
		string GetLinkNameById(int id);
		string GetDefaultLink(string typeName, string parentTypeName);
		string GetDefaultState(string typeName);

		/// <summary>
		/// Проверяет является ли тип документов.
		/// </summary>
		/// <param name="typeObj">Наименование типа.</param>
		/// <returns>True - если тип является документов.</returns>
		bool IsDocument(string typeObj);

		/// <summary>
		/// Проверяет является ли тип документов.
		/// </summary>
		/// <param name="typeId">Идентификатор типа.</param>
		/// <returns>True - если тип является документов.</returns>
		bool IsDocument(int typeId);

		IPluginPdmVersions LoadPdmVersionsByIds(string aIds);
	}
}