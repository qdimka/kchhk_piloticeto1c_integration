﻿using System;

namespace Ascon.AppPlugin
{
    class AppCacheWrapper : IAppCache
    {
        private readonly IPluginAppCache _appCache;

        public AppCacheWrapper(IPluginAppCache appCache)
        {
            if (appCache == null)
                throw new ArgumentNullException("appCache");
            this._appCache = appCache;
        }

        private void ApiCheck(bool res)
        {
            if (!res)
                throw new Exception(_appCache.GetLastError());
        }

        public string GetDbName()
        {
            return _appCache.GetDbName();
        }

        public string GetUserName()
        {
            return _appCache.GetUserName();
        }

        public string GetUserFullName()
        {
            return _appCache.GetUserFullName();
        }

        public IPluginDataSet GetTypes()
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetTypes(out res));
            return res;
        }

        public IPluginDataSet GetStates()
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetStates(out res));
            return res;    
        }

        public IPluginDataSet GetDbProperties()
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetDbProperties(out res));
            return res;               
        }

        public IPluginDataSet GetLinks()
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetLinks(out res));
            return res;               
        }

        public IPluginDataSet GetAttributeList()
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetAttributeList(out res));
            return res;               
        }

        public IPluginDataSet GetUserList()
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetUserList(out res));
            return res;               
        }

        public IPluginDataSet GetInfoAboutType(string typeName, int inMode)
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetInfoAboutType(typeName, inMode, out res));
            return res;               
        }

        public IPluginDataSet GetLinkAttrForTypes(string parentType, string typeObj, string linkType)
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetLinkAttrForTypes(parentType, typeObj, linkType, out res));
            return res;               
        }

        public IPluginDataSet GetInfoAboutCard(string cardName, int inMode)
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetInfoAboutCard(cardName, inMode, out res));
            return res;              
        }

        public IPluginDataSet GetInfoAboutAttribute(string attrName, int inMode)
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetInfoAboutAttribute(attrName, inMode, out res));
            return res;               
        }

        public IPluginDataSet GetInfoAboutCurrentBase(int inMode)
        {
            IPluginDataSet res;
            ApiCheck(_appCache.GetInfoAboutCurrentBase(inMode, out res));
            return res;               
        }

        public string GetDefaultLink(string typeName, string parentTypeName)
        {
            string res;
            ApiCheck(_appCache.GetDefaultLink(typeName, parentTypeName, out res));
            return res;   
        }

        public string GetDefaultState(string typeName)
        {
            string res;
            ApiCheck(_appCache.GetDefaultState(typeName, out res));
            return res;  
        }

        public bool IsDocument(string typeObj)
        {
            bool res;
            ApiCheck(_appCache.IsDocument(typeObj, out res));
            return res;  
        }

        public bool IsDocument(int typeId)
        {
            bool res;
            ApiCheck(_appCache.IsDocument(typeId, out res));
            return res;  
        }

        public string GetTypeNameById(int id)
        {
            string res;
            ApiCheck(_appCache.GetTypeNameById(id, out res));
            return res;  
        }

        public string GetStateNameById(int id)
        {
            string res;
            ApiCheck(_appCache.GetStateNameById(id, out res));
            return res;  
        }

        public string GetLinkNameById(int id)
        {
            string res;
            ApiCheck(_appCache.GetLinkNameById(id, out res));
            return res;  
        }

        public IPluginPdmVersions LoadPdmVersionsByIds(string aIds)
        {
            IPluginPdmVersions res;
            ApiCheck(_appCache.LoadPdmVersionsByIds(aIds, out res));
            return res;  
        }
		
		public IPluginDataSet GetAutoOperationList()
		{
			IPluginDataSet res;
			ApiCheck(_appCache.GetAutoOperationList(out res));
			return res;
		}
    }
}
