﻿using System;

namespace Ascon.AppPlugin
{
    class AppConfiguratorWrapper : IAppConfigurator
    {
        private readonly IPluginAppConfigurator _appConfigurator;

        public AppConfiguratorWrapper(IPluginAppConfigurator appConfigurator)
        {
            if (appConfigurator == null) 
                throw new ArgumentNullException("appConfigurator");
            this._appConfigurator = appConfigurator;
        }

        private void ApiCheck(bool res)
        {
            if (!res)
                throw new Exception(_appConfigurator.GetLastError());
        }

        public IPluginDataSet GetSortCase(string stProfile, string stType, string stLink, int inIndex)
        {
            IPluginDataSet ds;
            ApiCheck(_appConfigurator.GetSortCase(stProfile, stType, stLink, inIndex, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetSortTypes(int inIdCase)
        {
            IPluginDataSet ds;
            ApiCheck(_appConfigurator.GetSortTypes(inIdCase, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetSortTree(int inIdCase)
        {
            IPluginDataSet ds;
            ApiCheck(_appConfigurator.GetSortTree(inIdCase, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetSortTable(int inIdCase)
        {
            IPluginDataSet ds;
            ApiCheck(_appConfigurator.GetSortTable(inIdCase, out ds)
                );

            return ds;
        }
    }
}
