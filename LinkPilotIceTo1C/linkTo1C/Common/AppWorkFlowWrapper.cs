﻿using System;

namespace Ascon.AppPlugin
{
	public class AppWorkFlowWrapper : IAppWorkFlow
	{
		private readonly IPluginAppWorkFlow _appWorkFlow;

		public AppWorkFlowWrapper(IPluginAppWorkFlow appWorkFlow)
		{
			if (appWorkFlow == null)
				throw new ArgumentNullException("appWorkFlow");

			this._appWorkFlow = appWorkFlow;
		}

		private void ApiCheck(bool res)
		{
			if (!res)
				throw new Exception(_appWorkFlow.GetLastError());
		}

		public int CreateTask(string subj, string text, int idVersion, int idRole, int idUser, DateTime datePlan, int criticalTerm, bool checkResult, TbProcessKindExp kind)
		{
			int res;
			ApiCheck(_appWorkFlow.CreateTask(subj, text, idVersion, idRole, idUser, datePlan, criticalTerm, checkResult, kind, out res));
			return res;
		}

		public void StartTask(int idRoute, int idRole, int idUser, string subj, DateTime datePlan)
		{
			ApiCheck(_appWorkFlow.StartTask(idRoute, idRole, idUser, subj, datePlan));
		}

		public int CreateTaskByDialog()
		{
			int res;
			ApiCheck(_appWorkFlow.CreateTaskByDialog(out res));
			return res;
		}

		public IPluginDataSet GetIncomingCorrespondence(int inRetMode)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetIncomingCorrespondence(inRetMode, out ds));
			return ds;
		}

		public IPluginDataSet GetOutcomingCorrespondence(int inRetMode)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetOutcomingCorrespondence(inRetMode, out ds));
			return ds;
		}

		public IPluginDataSet GetCorrespondenceList(int inIdRoute)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetCorrespondenceList(inIdRoute, out ds));
			return ds;
		}

		public void DelLetter(int inIdLetter)
		{
			ApiCheck(_appWorkFlow.DelLetter(inIdLetter));
		}

		public IPluginDataSet GetUserList()
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetUserList(out ds));
			return ds;
		}

		public IPluginDataSet GetRoleTree(int inMode)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetRoleTree(inMode, out ds));
			return ds;
		}

		public IPluginDataSet GetProcessListByObject(string stDbName, int inPdmId)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetProcessListByObject(stDbName, inPdmId, out ds));
			return ds;
		}

		public IPluginDataSet GetProcessListByObjectEx(int inPdmId, string stDbName)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetProcessListByObjectEx(inPdmId, stDbName, out ds));
			return ds;
		}

		public IPluginDataSet GetInitialProcessList()
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetInitialProcessList(out ds));
			return ds;
		}

		public IPluginDataSet GetInfoAboutCurrentUser()
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetInfoAboutCurrentUser(out ds));
			return ds;
		}

		public IPluginDataSet GetInfoAboutRoute(int inIdRoute)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetInfoAboutRoute(inIdRoute, out ds));
			return ds;
		}

		public IPluginDataSet GetRoleListForUser(int inIdUser)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetRoleListForUser(inIdUser, out ds));
			return ds;
		}

		public IPluginDataSet GetTaskListForUser(int inIdUser, int inUserRoleMask, int inProcessStateMask, int inStageStateMode)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetTaskListForUser(inIdUser, inUserRoleMask, inProcessStateMask, inStageStateMode, out ds));
			return ds;
		}

		public int CreateNewProcess(string stDbName, int inIdPdmObject, string stRouteName)
		{
			int res;
			ApiCheck(_appWorkFlow.CreateNewProcess(stDbName, inIdPdmObject, stRouteName, out res));
			return res;
		}

		public void DeleteProcess(int inIdRoute)
		{
			ApiCheck(_appWorkFlow.DeleteProcess(inIdRoute));
		}

		public void InsUpdStageIntoBp(string stTask, int inCriticalTerm, DateTime dtDatePlan, int inCanControl, int inActive,
			int inIdRole, int inIdUser, int inIdBProc, int inCanTransform, ref int inIdStage, ref int inTimeConstraint, ref int inFromBpStart)
		{
			ApiCheck(_appWorkFlow.InsUpdStageIntoBp(stTask, inCriticalTerm, dtDatePlan, inCanControl, inActive, inIdRole, inIdUser, inIdBProc, inCanTransform, ref inIdStage, ref inTimeConstraint, ref inFromBpStart));
		}

		public void AddRouteItemsSeq(int inIdFirst, int inIdSecond, int inType)
		{
			ApiCheck(_appWorkFlow.AddRouteItemsSeq(inIdFirst, inIdSecond, inType));
		}

		public void AddRouteItemsRetSeq(int inIdFirst, int inIdSecond)
		{
			ApiCheck(_appWorkFlow.AddRouteItemsRetSeq(inIdFirst, inIdSecond));
		}

		public IPluginDataSet GetRoleListIntoProcess(int inIdRoute)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetRoleListIntoProcess(inIdRoute, out ds));
			return ds;
		}

		public void SetStateOfProcess(int inIdRoute, int inStateBp)
		{
			ApiCheck(_appWorkFlow.SetStateOfProcess(inIdRoute, inStateBp));
		}

		public void InsUpdUser(string stUserName, string stDescr, string stEMail, int inIsAdmin, ref int vaIdUser)
		{
			ApiCheck(_appWorkFlow.InsUpdUser(stUserName, stDescr, stEMail, inIsAdmin, ref vaIdUser));
		}

		public void SetTaskState(int inIdRoute, int inIdOperation, int inStateTask, string stMessageBody)
		{
			ApiCheck(_appWorkFlow.SetTaskState(inIdRoute, inIdOperation, inStateTask, stMessageBody));
		}

		public void SetItemState(int inIdItem, int inState, int inAddSignature)
		{
			ApiCheck(_appWorkFlow.SetItemState(inIdItem, inState, inAddSignature));
		}

		public void SetItemStateInternal(int inIdItem, int inState)
		{
			ApiCheck(_appWorkFlow.SetItemStateInternal(inIdItem, inState));
		}

		public void AddBpDocument(int inIdRoute, int inIdDoc, int inIdPar)
		{
			ApiCheck(_appWorkFlow.AddBpDocument(inIdRoute, inIdDoc, inIdPar));
		}

		public void DelBpDocument(int inIdRoute, int inIdDoc)
		{
			ApiCheck(_appWorkFlow.DelBpDocument(inIdRoute, inIdDoc));
		}

		public IPluginDataSet GetFirstItems(int inIdRoute)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetFirstItems(inIdRoute, out ds));
			return ds;
		}

		public IPluginDataSet GetNextItems(int inIdItem)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetNextItems(inIdItem, out ds));
			return ds;
		}

		public IPluginDataSet rep_ExecutedJobs(int inIdParent)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.rep_ExecutedJobs(inIdParent, out ds));
			return ds;
		}

		public IPluginDataSet GetJobsListIntoProcess(int inIdRoute)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetJobsListIntoProcess(inIdRoute, out ds));
			return ds;
		}

		public IPluginDataSet GetAttachDocumentListIntoProcess(int inIdRoute)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetAttachDocumentListIntoProcess(inIdRoute, out ds));
			return ds;
		}

		public void StartNextStage(int inIdRoute, int inIdItem, int inDirect)
		{
			ApiCheck(_appWorkFlow.StartNextStage(inIdRoute, inIdItem, inDirect));
		}

		public void RenameProcess(int inIdRoute, string stNewName)
		{
			ApiCheck(_appWorkFlow.RenameProcess(inIdRoute, stNewName));
		}

		public void SetStageExecutor(int inIdStage, int inIdUser, int inIdRole)
		{
			ApiCheck(_appWorkFlow.SetStageExecutor(inIdStage, inIdUser, inIdRole));
		}

		public void SetStageTerm(int inIdStage, int inTimeLimit, DateTime vaPlanEnd, int inTimeConstraint, int inFromBpStart)
		{
			ApiCheck(_appWorkFlow.SetStageTerm(inIdStage, inTimeLimit, vaPlanEnd, inTimeConstraint, inFromBpStart));
		}

		public void SetStageTask(int inIdStage, string stTask)
		{
			ApiCheck(_appWorkFlow.SetStageTask(inIdStage, stTask));
		}

		public void SetBP_PDMObject(int inIdRoute, int inIdPdmObject)
		{
			ApiCheck(_appWorkFlow.SetBP_PDMObject(inIdRoute, inIdPdmObject));
		}

		public IPluginDataSet GetTaskListForObject(int inIdVersion, int inProcessStateMask, int inStageStateMode)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetTaskListForObject(inIdVersion, inProcessStateMask, inStageStateMode, out ds));
			return ds;
		}

		public IPluginDataSet GetInfoAboutStage(int inIdStage)
		{
			IPluginDataSet ds;
			ApiCheck(_appWorkFlow.GetInfoAboutStage(inIdStage, out ds));
			return ds;
		}

		public void GetVariable(int inIdRoute, string stName, out int res)
		{
			ApiCheck(_appWorkFlow.GetVariable(inIdRoute, stName, out res));
		}

		public void GetVariable(int inIdRoute, string stName, out string res)
		{
			ApiCheck(_appWorkFlow.GetVariable(inIdRoute, stName, out res));
		}

		public void GetVariable(int inIdRoute, string stName, out DateTime res)
		{
			ApiCheck(_appWorkFlow.GetVariable(inIdRoute, stName, out res));
		}

		public void GetVariable(int inIdRoute, string stName, out double res)
		{
			ApiCheck(_appWorkFlow.GetVariable(inIdRoute, stName, out res));
		}

		public void SetVariable(int inIdRoute, string stName, int value, bool readOnlyForChild)
		{
			ApiCheck(_appWorkFlow.SetVariable(inIdRoute, stName, value, readOnlyForChild));
		}

		public void SetVariable(int inIdRoute, string stName, string value, bool readOnlyForChild)
		{
			ApiCheck(_appWorkFlow.SetVariable(inIdRoute, stName, value, readOnlyForChild));
		}

		public void SetVariable(int inIdRoute, string stName, DateTime value, bool readOnlyForChild)
		{
			ApiCheck(_appWorkFlow.SetVariable(inIdRoute, stName, value, readOnlyForChild));
		}

		public void SetVariable(int inIdRoute, string stName, double value, bool readOnlyForChild)
		{
			ApiCheck(_appWorkFlow.SetVariable(inIdRoute, stName, value, readOnlyForChild));
		}

		public int SendLetterByBpToOneUser(int inIdRoute, int inIdAuRole, int inIdRecRole, int inIdRecUser, string stSubj, string stBody)
		{
			int res;
			ApiCheck(_appWorkFlow.SendLetterByBpToOneUser(inIdRoute, inIdAuRole, inIdRecRole, inIdRecUser, stSubj, stBody, out res));
			return res;
		}

		public int SendLetterByBpToUsers(int inIdRoute, int inIdAuRole, int inIdRecRole, int inIdRecUser, string stSubj, string stBody)
		{
			int result;
			ApiCheck(_appWorkFlow.SendLetterByBpToOneUser(inIdRoute, inIdAuRole, inIdRecRole, inIdRecUser, stSubj, stBody, out result));
			return result;
		}

		public IPluginDataSet GetTypicalProcessList(int inMode)
		{
			IPluginDataSet res;
			ApiCheck(_appWorkFlow.GetTypicalProcessList(inMode, out res));
			return res;
		}

		public int CreateNewProcessBy(string stDbName, int inIdPdmObject, string stRouteName, int inIdBaseRoute)
		{
			int res;
			ApiCheck(_appWorkFlow.CreateNewProcessBy(stDbName, inIdPdmObject, stRouteName, inIdBaseRoute, out res));
			return res;
		}

		public int InsUpdOpInstance(int inIdOperDescr, int inIsWait, int inIdProc, int inIdOperInst)
		{
			int res;
			ApiCheck(_appWorkFlow.InsUpdOpInstance(inIdOperDescr, inIsWait, inIdProc, inIdOperInst, out res));
			return res;
		}
	}
}