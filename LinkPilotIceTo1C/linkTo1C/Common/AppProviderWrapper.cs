﻿using System;

namespace Ascon.AppPlugin
{
    class AppProviderWrapper : IAppProvider
    {
        private readonly IPluginAppProvider _appProvider;
        private readonly IAppMain _appMain;
        private readonly IAppWorkFlow _appWorkFlow;
        private readonly IAppCache _appCache;
        private readonly IAppConfigurator _appConfigurator;

        public AppProviderWrapper(IPluginAppProvider appProvider)
        {
            if (appProvider == null)
                throw new ArgumentNullException("appProvider");
            this._appProvider = appProvider;
            _appMain = new AppMainWrapper(appProvider.GetAppMain());
            _appWorkFlow = new AppWorkFlowWrapper(appProvider.GetAppWorkFlow());
            _appCache = new AppCacheWrapper(appProvider.GetAppCache());
            _appConfigurator = new AppConfiguratorWrapper(appProvider.GetAppConfigurator());
        }

        private void ApiCheck(bool res)
        {
            if (!res)
                throw new Exception(_appProvider.GetLastError());
        }

        public IAppMain AppMain => _appMain;

        public IAppCache AppCache => _appCache;

        public IAppWorkFlow AppWorkFlow => _appWorkFlow;

        public IAppConfigurator AppConfigurator => _appConfigurator;

        public string GetAppTitle()
        {
            return _appProvider.GetAppTitle();
        }

        public int GetAppHandle()
        {
            return _appProvider.GetAppHandle();
        }

        public IPluginPdmVersions GetSelectedObjects()
        {
            return _appProvider.GetSelectedObjects();
        }

        public void RefreshVersion(int idVersion)
        {
            _appProvider.RefreshVersion(idVersion);
        }

        public void SelectObject(int idVersion)
        {
            ApiCheck(_appProvider.SelectObject(idVersion));
        }

        public void SelectTask(int idProcess, TasksFilterExp tasksFilter)
        {
            ApiCheck(_appProvider.SelectTask(idProcess, tasksFilter));
        }

        public void SelectMail(int idLetter, MailFilterExp filter)
        {
            ApiCheck(_appProvider.SelectMail(idLetter, filter));
        }

        public IPluginTasks GetSelectedTasks()
        {
            IPluginTasks res;
            ApiCheck(_appProvider.GetSelectedTasks(out res));
            return res;
        }

        public ModalResultExp GetAttachFromDlg(out IPluginPdmVersions versions)
        {
            ModalResultExp res;
            ApiCheck(_appProvider.GetAttachFromDlg(out versions, out res));
            return res;
        }

        public ModalResultExp GetUsersFromDlg(bool aDateVisible, bool aSignRoleVisible, bool aMultiselect, bool aCanIAm, IPluginUsers aSelectedUsers, out IPluginUsers user)
        {
            ModalResultExp res;
            ApiCheck(_appProvider.GetUsersFromDlg(aDateVisible, aSignRoleVisible, aMultiselect, aCanIAm, aSelectedUsers, out user, out res));
            return res;
        }

        public void RefreshTasks(int aSelectedRouteId)
        {
            ApiCheck(_appProvider.RefreshTasks(aSelectedRouteId));
        }
    }
}
