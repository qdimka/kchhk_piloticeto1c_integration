﻿using System;

namespace Ascon.AppPlugin
{
    class AppMainWrapper : IAppMain
    {
        private readonly IPluginAppMain _appMain;
        
        public AppMainWrapper(IPluginAppMain appMain)
        {
            if (appMain == null) 
                throw new ArgumentNullException("appMain");
            this._appMain = appMain;
        }

        private void ApiCheck(bool res)
        {
            if (!res)
                throw new Exception(_appMain.GetLastError());
        }

        public void Client_CreateFile(string targetFile, int idCheckout, int idVersion, string fileName)
        {
            ApiCheck(_appMain.Client_CreateFile(targetFile, idCheckout, idVersion, fileName));
        }

        public void Client_DeleteFile(int idCheckout, int idVersion, string fileName)
        {
            ApiCheck(_appMain.Client_DeleteFile(idCheckout, idVersion, fileName));
        }

        public void Client_UpdateFile(string targetFile, int idCheckout, int idVersion, string fileName)
        {
            ApiCheck(_appMain.Client_UpdateFile(targetFile, idCheckout, idVersion, fileName));
        }

        public string Client_GetFilePath(int idVersion, string stFileName)
        {
            string path;
            ApiCheck(_appMain.Client_GetFilePath(idVersion, stFileName, out path)
                );

            return path;
        }

        public string Client_GetFile(int idCheckout, string stTypeObj, string stProduct, string stVersion, string stFileName, string stLocalName, int inSize, int inCrc, DateTime dtCreate, DateTime dtModify, bool revertLocalChanges)
        {
            string path;
            ApiCheck(_appMain.Client_GetFile(idCheckout, stTypeObj, stProduct, stVersion, stFileName, stLocalName, inSize, inCrc, dtCreate, dtModify, revertLocalChanges, out path)
                );

            return path;
        }

        public string Client_GetFileById(int idCheckout, int idVersion, string stFileName, string stLocalName, int inSize, int inCrc, DateTime dtCreate, DateTime dtModify, bool revertLocalChanges)
        {
            string path;
            ApiCheck(_appMain.Client_GetFileById(idCheckout, idVersion, stFileName, stLocalName, inSize, inCrc, dtCreate, dtModify, revertLocalChanges, out path)
                );

            return path;
        }

        public IPluginDataSet CheckUniqueName(string stTypeName, string stProductName)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.CheckUniqueName(stTypeName, stProductName, out ds)
                );

            return ds;
        }

        public int ExistsObject(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, int inIdVersion)
        {
            int res;
            ApiCheck(_appMain.ExistsObject(idCheckOut, stTypeName, stProductName, stVersionNumber, inIdVersion, out res));

            return res;
        }

        public IPluginDataSet FindObjects(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, string stStateName, string stAttrCondition, string stProjects, string stLinkType)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.FindObjects(idCheckOut, stTypeName, stProductName, stVersionNumber, stStateName, stAttrCondition, stProjects, stLinkType, out ds)
                );

            return ds;
        }

        public IPluginDataSet FindObjectsInContext2(int idCheckOut, string stContext, string stSearchXml, int inParams)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.FindObjectsInContext2(idCheckOut, stContext, stSearchXml, inParams, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetAllLinkedObjects(int idCheckOut, string ids, int inParams)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetAllLinkedObjects(idCheckOut, ids, inParams, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetAttributeList()
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetAttributeList(out ds)
                );

            return ds;
        }

        public IPluginDataSet GetAttributesValues2(string stIds, string stAttrIds)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetAttributesValues2(stIds, stAttrIds, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetAttrImageValueById(int idCheckOut, int inIdVersion, string stAttrName, int inIdLink)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetAttrImageValueById(idCheckOut, inIdVersion, stAttrName, inIdLink, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutAttribute(string stAttrName, int inMode)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutAttribute(stAttrName, inMode, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutCard(string stCardName, int inMode)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutCard(stCardName, inMode, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutCurrentBase(int inMode)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutCurrentBase(inMode, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutCurrentUser()
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutCurrentUser(out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutLink(int idCheckOut, int inIdLink, int inMode)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutLink(idCheckOut, inIdLink, inMode, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutType(string stTypeName, int inMode)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutType(stTypeName, inMode, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutUserSet(string stUserSet, string stDbName, int inMode)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutUserSet(stUserSet, stDbName, inMode, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutVersion(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, int inIdVersion, int inMode)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutVersion(idCheckOut, stTypeName, stProductName, stVersionNumber, inIdVersion, inMode, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutVersionsFiles(int idCheckOut, string stIds)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutVersionsFiles(idCheckOut, stIds, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetInfoAboutVersionsPrivileges(string stIds)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetInfoAboutVersionsPrivileges(stIds, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLinkAttrForTypes(string stParentType, string stChildType, string stLinkType)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLinkAttrForTypes(stParentType, stChildType, stLinkType, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLinkAttributes(int idCheckOut, int inIdLink)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLinkAttributes(idCheckOut, inIdLink, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLinkedFast(int idCheckOut, int inIdVersion, string stLinkType, bool boInverse)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLinkedFast(idCheckOut, inIdVersion, stLinkType, boInverse, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLinkedFast2(int idCheckOut, int inIdVersion, string stLinkType, bool boInverse)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLinkedFast2(idCheckOut, inIdVersion, stLinkType, boInverse, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLinkedObjects2(int idCheckOut, int inIdVersion, string stLinkType, bool boInverse, bool boFullLink, bool boGroupByProduct)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLinkedObjects2(idCheckOut, inIdVersion, stLinkType, boInverse, boFullLink, boGroupByProduct, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLinkedObjectsAndFiles(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, string stLinkType, bool boFullLink, bool boViewOnlyDocuments)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLinkedObjectsAndFiles(idCheckOut, stTypeName, stProductName, stVersionNumber, stLinkType, boFullLink, boViewOnlyDocuments, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLinkedObjectsEx(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, int inIdVersion, string stLinkType, bool boInverse, bool boFullLink, bool boGroupByProduct)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLinkedObjectsEx(idCheckOut, stTypeName, stProductName, stVersionNumber, inIdVersion, stLinkType, boInverse, boFullLink, boGroupByProduct, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLinks(int idCheckOut, int inIdVersion1, int inIdVersion2)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLinks(idCheckOut, inIdVersion1, inIdVersion2, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLObjs(int idCheckOut, int inIdVersion, bool boInverse)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLObjs(idCheckOut, inIdVersion, boInverse, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetLockedObjects(int idCheckOut, int inParams)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetLockedObjects(idCheckOut, inParams, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetPrivilegesForObjects(int idCheckOut, string stIds, int mode)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetPrivilegesForObjects(idCheckOut, stIds, mode, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetProjectList2(int idCheckOut, int inParams)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetProjectList2(idCheckOut, inParams, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetProjectListEx(int idCheckOut, bool boWithAttrs)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetProjectListEx(idCheckOut, boWithAttrs, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetPropObjects2(int idCheckOut, string stObjects, int inParams)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetPropObjects2(idCheckOut, stObjects, inParams, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetPropObjects3(int idCheckOut, string ids, string stAttrIds)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetPropObjects3(idCheckOut, ids, stAttrIds, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetStateList()
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetStateList(out ds)
                );

            return ds;
        }

        public IPluginDataSet GetTree(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, int inIdVersion, string stLinkType, bool boWithAttrs)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetTree(idCheckOut, stTypeName, stProductName, stVersionNumber, inIdVersion, stLinkType, boWithAttrs, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetTree2(int idCheckOut, int inIdVersion, string stLinkType, int inParams)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetTree2(idCheckOut, inIdVersion, stLinkType, inParams, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetTypeList()
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetTypeList(out ds)
                );

            return ds;
        }

        public IPluginDataSet GetUserGroupList()
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetUserGroupList(out ds)
                );

            return ds;
        }

        public IPluginDataSet GetUserList()
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetUserList(out ds)
                );

            return ds;
        }

        public IPluginDataSet GetVersionList(int idCheckOut, string stTypeName, string stProductName)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetVersionList(idCheckOut, stTypeName, stProductName, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetVersionList2(int idCheckOut, int inIdType, string stProductName)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetVersionList2(idCheckOut, inIdType, stProductName, out ds)
                );

            return ds;
        }

        public int InsertObject(int idCheckOut, string stParentType, string stParentProduct, string stParentVersion, string stChildType, string stChildProduct, string stChildVersion, string stLinkType, string stState, bool boKeyInsert)
        {
            int res;
            ApiCheck(_appMain.InsertObject(idCheckOut, stParentType, stParentProduct, stParentVersion, stChildType, stChildProduct, stChildVersion, stLinkType, stState, boKeyInsert, out res)
                );

            return res;
        }

        public bool IsProject(string stTypeName, string stProductName, bool boIsProject)
        {
            return _appMain.IsProject(stTypeName, stProductName, boIsProject);
        }

        public void KillVersionById(int idCheckOut, int inIdVersion)
        {
            ApiCheck(_appMain.KillVersionById(idCheckOut, inIdVersion)
                );
        }

        public IPluginDataSet KillVersions(int idCheckOut, string stIds, int inParams)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.KillVersions(idCheckOut, stIds, inParams, out ds)
                );

            return ds;
        }

        public int NewObject(int idCheckOut, string stTypeName, string stStateName, string stProductName, int inProject)
        {
            int res;
            ApiCheck(_appMain.NewObject(idCheckOut, stTypeName, stStateName, stProductName, inProject, out res)
                );

            return res;
        }

        public void NewUserSet(string stUserSet, string stDbName, string stParentUserSet)
        {
            ApiCheck(_appMain.NewUserSet(stUserSet, stDbName, stParentUserSet)
                );
        }

        public IPluginDataSet OpenUserSet2(string stUserSet, int inParams)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.OpenUserSet2(stUserSet, inParams, out ds)
                );

            return ds;
        }

        public void UpdateStateOnObjectById(int idCheckOut, int inIdVersion, string stStateName)
        {
            ApiCheck(_appMain.UpdateStateOnObjectById(idCheckOut, inIdVersion, stStateName)
                );
        }

        public void UpFindPattern(string stFindPattern, string stUserSet, string stDbName, string stTypePattern, string stProductPattern, string stVersionPattern, string stStatePattern, string stAttrCondition, string stProjectPattern, int idPattern, bool boActive, bool boDel)
        {
            ApiCheck(_appMain.UpFindPattern(stFindPattern, stUserSet, stDbName, stTypePattern, stProductPattern, stVersionPattern, stStatePattern, stAttrCondition, stProjectPattern, idPattern, boActive, boDel)
                );
        }

        public void UpGrantOnVersion(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, string stOrgUnitName, int inPriveleges, DateTime dtFinishTime, bool boDel)
        {
            ApiCheck(_appMain.UpGrantOnVersion(idCheckOut, stTypeName, stProductName, stVersionNumber, stOrgUnitName, inPriveleges, dtFinishTime, boDel)
                );
        }

        public int UpLink(int idCheckOut, string stParentType, string stParentProduct, string stParentVersion, string stChildType, string stChildProduct, string stChildVersion, int inIdLink, string stIdUnit, bool boDel, string stLinkType)
        {
            int res;
            ApiCheck(_appMain.UpLink(idCheckOut, stParentType, stParentProduct, stParentVersion, stChildType, stChildProduct, stChildVersion, inIdLink, stIdUnit, boDel, stLinkType, out res)
                );

            return res;
        }

        public int UpObject(int idCheckOut, string stTypeName, string stProductName, string stNewProductName)
        {
            int res;
            ApiCheck(_appMain.UpObject(idCheckOut, stTypeName, stProductName, stNewProductName, out res)
                );

            return res;
        }

        public int CheckOut(string stTypeName, string stProductName, string stVersionNumber, int inMode)
        {
            int res;
            ApiCheck(_appMain.CheckOut(stTypeName, stProductName, stVersionNumber, inMode, out res)
                );

            return res;
        }

        public void CheckIn(int idCheckOut)
        {
            ApiCheck(_appMain.CheckIn(idCheckOut)
                );
        }

        public void CancelCheckOut(int idCheckOut)
        {
            ApiCheck(_appMain.CancelCheckOut(idCheckOut)
                );
        }

        public void UpAttrValue(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, string stAttrName, int vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpAttrValue(idCheckOut, stTypeName, stProductName, stVersionNumber, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpAttrValue(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, string stAttrName, string vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpAttrValue(idCheckOut, stTypeName, stProductName, stVersionNumber, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpAttrValue(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, string stAttrName, DateTime vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpAttrValue(idCheckOut, stTypeName, stProductName, stVersionNumber, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpAttrValue(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, string stAttrName, double vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpAttrValue(idCheckOut, stTypeName, stProductName, stVersionNumber, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpAttrValueById(int idCheckOut, int inIdVersion, string stAttrName, int vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpAttrValueById(idCheckOut, inIdVersion, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpAttrValueById(int idCheckOut, int inIdVersion, string stAttrName, string vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpAttrValueById(idCheckOut, inIdVersion, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpAttrValueById(int idCheckOut, int inIdVersion, string stAttrName, DateTime vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpAttrValueById(idCheckOut, inIdVersion, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpAttrValueById(int idCheckOut, int inIdVersion, string stAttrName, double vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpAttrValueById(idCheckOut, inIdVersion, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpLinkAttrValue(int idCheckOut, int inIdLink, string stAttrName, int vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpLinkAttrValue(idCheckOut, inIdLink, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpLinkAttrValue(int idCheckOut, int inIdLink, string stAttrName, string vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpLinkAttrValue(idCheckOut, inIdLink, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpLinkAttrValue(int idCheckOut, int inIdLink, string stAttrName, DateTime vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpLinkAttrValue(idCheckOut, inIdLink, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public void UpLinkAttrValue(int idCheckOut, int inIdLink, string stAttrName, double vaAttrValue, string stIdUnit, bool boDel)
        {
            ApiCheck(_appMain.UpLinkAttrValue(idCheckOut, inIdLink, stAttrName, vaAttrValue, stIdUnit, boDel)
                );
        }

        public string GetDbList()
        {
            string res;
            ApiCheck(_appMain.GetDbList(out res)
                );

            return res;
        }

        public IPluginDataSet GetSignatures(int aIdCheckout, int aVersionId)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetSignatures(aIdCheckout, aVersionId, out ds)
                );

            return ds;
        }

        public IPluginDataSet GetAnnotations(int aIdCheckout, int aVersionId)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetAnnotations(aIdCheckout, aVersionId, out ds)
                );

            return ds;
        }

        public void SetUserSetPattern(string userSetName, string dbName, string xmlPattern)
        {
            ApiCheck(_appMain.SetUserSetPattern(userSetName, dbName, xmlPattern)
                );
        }

        public void UpUserSet(string stUserSet, string stDbName, string stNewUserSet, string stGroupName, int inGroupOperation, bool boDel)
        {
            ApiCheck(_appMain.UpUserSet(stUserSet, stDbName, stNewUserSet, stGroupName, inGroupOperation, boDel)
                );
        }

        public IPluginDataSet GetReport(int idCheckOut, string reportName, string Params)
        {
            IPluginDataSet ds;
            ApiCheck(_appMain.GetReport(idCheckOut, reportName, Params, out ds)
                );

            return ds;
        }

        public int NewVersionEx(int idCheckOut, string stTypeName, string stProductName, string stVersionNumber, string stStateName, string vaFileList, int inGroup, int inParams)
        {
            int res;
            ApiCheck(_appMain.NewVersionEx(idCheckOut, stTypeName, stProductName, stVersionNumber, stStateName, vaFileList, inGroup, inParams, out res)
                );

            return res;
        }

        public void Loodsman_ExtractFile(int idCheckout, string typeObj, string product, string version, int idVersion, string stFileName, string stLocalName, int inSize, int inCrc, DateTime dtCreate, DateTime dtModify, string targetFile)
        {
            ApiCheck(_appMain.Loodsman_ExtractFile(idCheckout, typeObj, product, version, idVersion, stFileName, stLocalName, inSize, inCrc, dtCreate, dtModify, targetFile)
                );
        }
    }
}
