﻿namespace Ascon.AppPlugin
{
    using System;
    using System.Runtime.InteropServices;
    using Proxy.Net.CrossPlatform;


    // Возможные результаты выполнения метода плагина
    public enum CallResult 
    {
                      CrOk,
                      CrError,
                      CrStop
    };

    // Уровень прав доступа к объекту
    public enum AccessLevelExp
    {
        AlUndefined, 
        AlNoAccess, 
        AlReadOnly, 
        AlReadWrite, 
        AlAdministration
    }

    // Фильтьры заданий отображанмых на вкладке "Задания"
    public enum TasksFilterExp
    {  
        TfActual, 
        TfPlanned, 
        TfSend, 
        TfIncomming, 
        TfOverdue,
        TfCompleted, 
        TfRevoked, 
        TfAll
    }

    // Фильтры сообщений отображаемых на вкладке "Сообщения"
    public enum MailFilterExp
    {
        MkIncoming,       // Входящие письма
        MkMailed,         // Отправленные
        MkDeleted         // Удаленные
    }

    public enum TbProcessKindExp
    {
        BpkTask,
        BpkTaskAgree,
        BpkTaskFamiliarization,
				BpkPluginTask,
        BpkUnknown
    }

    public enum TaskKindExp
    {
        TkIncomming,
        TkOutgoing
    }

    public enum TaskStateExp
    {
        TsUnknown,
        TsPlanned,
        TsSend,
        TsInWork,
        TsRevoked,
        TsInChecked,
        TsComleted
    }

    public enum ModalResultExp
    {
        MreNone,
        MreOk,
        MreCancel
    }


    //-------------------------------------------------------
    // Интерфейсы предоставляемые главным приложением
    //-------------------------------------------------------

    [Guid("b2dc9772-cf76-4f20-b423-25b62cebc321")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginDataSet
    {
        int RecordCount { get; }
        string Filter { get; set; }
        bool Eof { get; }
        bool Filtered { get; set; }
        int FieldCount { get; }
        string Field(int index);

        void First();
        void Next();
        void Last();
        bool Locate(string keyFields, string keyValues);

        string AsString(string fieldName);
        int AsInteger(string fieldName);
        DateTime AsDateTime(string fieldName);
        float AsFloat(string fieldName);
    }


    [Guid("4ef6fd95-63ad-4e73-b028-b9ae93b74295")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginAppCache
    {
        string GetLastError();
        string GetDbName();
        string GetUserName();
        string GetUserFullName();

        bool GetTypes(out IPluginDataSet ds);
        bool GetStates(out IPluginDataSet ds);
        bool GetDbProperties(out IPluginDataSet ds);
        bool GetLinks(out IPluginDataSet ds);
        bool GetAttributeList(out IPluginDataSet ds);
        bool GetUserList(out IPluginDataSet ds);
        bool GetInfoAboutType(string typeName, int inMode, out IPluginDataSet ds);
        /// <summary>Аналог стандартной GetLinkAttrForTypes.</summary>
        /// <returns>Возвращает возможные атрибуты связей для связки типов.</returns>
        bool GetLinkAttrForTypes(string parentType, string typeObj, string linkType, out IPluginDataSet ds);
        bool GetInfoAboutCard(string cardName, int inMode, out IPluginDataSet ds);
        bool GetInfoAboutAttribute(string attrName, int inMode, out IPluginDataSet ds);
        bool GetInfoAboutCurrentBase(int inMode, out IPluginDataSet ds);

        bool GetDefaultLink(string typeName, string parentTypeName, out string res);
        bool GetDefaultState(string typeName, out string res);

        /// <summary>Проверяет является ли тип документов.</summary>
        /// <param name="typeObj">Наименование типа.</param>
        /// <param name="res"></param>
        /// <returns>True - если тип является документов.</returns>
        bool IsDocument(string typeObj, out bool res);

        /// <summary>Проверяет является ли тип документов.</summary>
        /// <param name="typeId">Идентификатор типа.</param>
        /// <param name="res"></param>
        /// <returns>True - если тип является документов.</returns>
        bool IsDocument(int typeId, out bool res);
        bool GetTypeNameById(int id, out string res);
        bool GetStateNameById(int id, out string res);
        bool GetLinkNameById(int id, out string res);
        bool LoadPdmVersionsByIds(string aIds, out IPluginPdmVersions res);
        bool GetAutoOperationList(out IPluginDataSet ds);
    }


    [Guid("e7595dd5-57e1-4f14-bde0-2f662dddb17a")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginAppMain
    {
        string GetLastError();
        
        // API-клиента для работы с файлами
        bool Client_CreateFile(string targetFile, int idCheckout, int idVersion,
            string fileName);
        bool Client_DeleteFile(int idCheckout, int idVersion,
            string fileName);
        bool Client_UpdateFile(string targetFile, int idCheckout, int idVersion,
            string fileName);
        bool Client_GetFilePath(int idVersion, string stFileName,
            out string path);
        bool Client_GetFile(int idCheckout, string stTypeObj, string stProduct, string stVersion,
            string stFileName, string stLocalName, int inSize, int inCrc,
            DateTime dtCreate, DateTime dtModify, bool revertLocalChanges,
            out string path);
        bool Client_GetFileById(int idCheckout, int idVersion,
            string stFileName, string stLocalName, int inSize, int inCrc,
            DateTime dtCreate, DateTime dtModify, bool revertLocalChanges,
            out string path);

        // API-ЛОЦМАН
        bool CheckUniqueName(string stTypeName, string stProductName,
          out IPluginDataSet ds);
        bool ExistsObject(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, int inIdVersion, out int res);
        bool FindObjects(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, string stStateName, string stAttrCondition, string stProjects,
          string stLinkType, out IPluginDataSet ds);
        bool FindObjectsInContext2(int idCheckOut,
          string stContext, string stSearchXml, int inParams,
          out IPluginDataSet ds);
        bool GetAllLinkedObjects(int idCheckOut, string ids,
         int inParams, out IPluginDataSet ds);
        bool GetAttributeList(out IPluginDataSet ds);
        bool GetAttributesValues2(string stIds, string stAttrIds,
          out IPluginDataSet ds);
        bool GetAttrImageValueById(int idCheckOut, int inIdVersion,
          string stAttrName, int inIdLink,
          out IPluginDataSet ds);
        bool GetInfoAboutAttribute(string stAttrName, int inMode,
          out IPluginDataSet ds);
        bool GetInfoAboutCard(string stCardName, int inMode,
          out IPluginDataSet ds);
        bool GetInfoAboutCurrentBase(int inMode,
          out IPluginDataSet ds);
        bool GetInfoAboutCurrentUser(out IPluginDataSet ds);
        bool GetInfoAboutLink(int idCheckOut, int inIdLink, int inMode,
          out IPluginDataSet ds);
        bool GetInfoAboutType(string stTypeName, int inMode,
          out IPluginDataSet ds);
        bool GetInfoAboutUserSet(string stUserSet, string stDbName,
         int inMode, out IPluginDataSet ds);
        bool GetInfoAboutVersion(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, int inIdVersion, int inMode,
          out IPluginDataSet ds);
        bool GetInfoAboutVersionsFiles(int idCheckOut, string stIds,
          out IPluginDataSet ds);
        bool GetInfoAboutVersionsPrivileges(string stIds,
          out IPluginDataSet ds);
        bool GetLinkAttrForTypes(string stParentType, string stChildType,
          string stLinkType, out IPluginDataSet ds);
        bool GetLinkAttributes(int idCheckOut, int inIdLink,
          out IPluginDataSet ds);
        bool GetLinkedFast(int idCheckOut, int inIdVersion, string stLinkType,
          bool boInverse, out IPluginDataSet ds);
        bool GetLinkedFast2(int idCheckOut, int inIdVersion, string stLinkType,
          bool boInverse, out IPluginDataSet ds);
        bool GetLinkedObjects2(int idCheckOut, int inIdVersion,
          string stLinkType, bool boInverse, bool boFullLink, bool boGroupByProduct,
          out IPluginDataSet ds);
        bool GetLinkedObjectsAndFiles(int idCheckOut,
          string stTypeName, string stProductName, string stVersionNumber, string stLinkType,
          bool boFullLink, bool boViewOnlyDocuments,
          out IPluginDataSet ds);
        bool GetLinkedObjectsEx(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, int inIdVersion, string stLinkType,
          bool boInverse, bool boFullLink, bool boGroupByProduct,
          out IPluginDataSet ds);
        bool GetLinks(int idCheckOut, int inIdVersion1, int inIdVersion2,
          out IPluginDataSet ds);
        bool GetLObjs(int idCheckOut, int inIdVersion, bool boInverse,
          out IPluginDataSet ds);
        bool GetLockedObjects(int idCheckOut, int inParams,
          out IPluginDataSet ds);
        bool GetPrivilegesForObjects(int idCheckOut, string stIds,
          int mode, out IPluginDataSet ds);
        bool GetProjectList2(int idCheckOut, int inParams,
          out IPluginDataSet ds);
        bool GetProjectListEx(int idCheckOut, bool boWithAttrs,
          out IPluginDataSet ds);
        bool GetPropObjects2(int idCheckOut, string stObjects,
          int inParams, out IPluginDataSet ds);
        bool GetPropObjects3(int idCheckOut, string ids, string stAttrIds,
          out IPluginDataSet ds);
        bool GetStateList(out IPluginDataSet ds);
        bool GetTree(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, int inIdVersion, string stLinkType,
          bool boWithAttrs, out IPluginDataSet ds);
        bool GetTree2(int idCheckOut, int inIdVersion, string stLinkType,
         int inParams, out IPluginDataSet ds);
        bool GetTypeList(out IPluginDataSet ds);
        bool GetUserGroupList(out IPluginDataSet ds);
        bool GetUserList(out IPluginDataSet ds);
        bool GetVersionList(int idCheckOut, string stTypeName,
          string stProductName, out IPluginDataSet ds);
        bool GetVersionList2(int idCheckOut, int inIdType,
          string stProductName, out IPluginDataSet ds);
        bool InsertObject(int idCheckOut, string stParentType, string stParentProduct,
          string stParentVersion, string stChildType, string stChildProduct, string stChildVersion, string stLinkType,
          string stState, bool boKeyInsert,
          out int res);
        bool IsProject(string stTypeName, string stProductName,
          bool boIsProject);
        bool KillVersionById(int idCheckOut, int inIdVersion);
        bool KillVersions(int idCheckOut, string stIds,
          int inParams, out IPluginDataSet ds);
        bool NewObject(int idCheckOut, string stTypeName, string stStateName,
          string stProductName, int inProject,
          out int res);
        bool NewUserSet(string stUserSet, string stDbName, string stParentUserSet);
        bool OpenUserSet2(string stUserSet, int inParams,
          out IPluginDataSet ds);
        bool UpdateStateOnObjectById(int idCheckOut, int inIdVersion,
          string stStateName);
        bool UpFindPattern(string stFindPattern, string stUserSet, string stDbName, string stTypePattern,
          string stProductPattern, string stVersionPattern, string stStatePattern, string stAttrCondition,
          string stProjectPattern, int idPattern,
          bool boActive, bool boDel);
        bool UpGrantOnVersion(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, string stOrgUnitName, int inPriveleges,
          DateTime dtFinishTime, bool boDel);
        bool UpLink(int idCheckOut, string stParentType, string stParentProduct,
          string stParentVersion, string stChildType, string stChildProduct, string stChildVersion,
          int inIdLink, /*reMinQuantity, reMaxQuantity: Variant;*/ string stIdUnit,
          bool boDel, string stLinkType,
          out int res);
        bool UpObject(int idCheckOut, string stTypeName, string stProductName,
          string stNewProductName, out int res);
        bool CheckOut(string stTypeName, string stProductName, string stVersionNumber,
          int inMode, out int res);
        bool CheckIn(int idCheckOut);
        bool CancelCheckOut(int idCheckOut);
        // перегруженные методы, отличаются типом параметра vaAttrValue 
        bool UpAttrValue(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, string stAttrName, int vaAttrValue,
          string stIdUnit, bool boDel);
        bool UpAttrValue(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, string stAttrName, string vaAttrValue,
          string stIdUnit, bool boDel);
        bool UpAttrValue(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, string stAttrName, DateTime vaAttrValue,
          string stIdUnit, bool boDel);
        bool UpAttrValue(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, string stAttrName, double vaAttrValue,
          string stIdUnit, bool boDel);
        // перегруженные методы, отличаются типом параметра vaAttrValue
        bool UpAttrValueById(int idCheckOut, int inIdVersion,
          string stAttrName, int vaAttrValue, string stIdUnit,
          bool boDel);
        bool UpAttrValueById(int idCheckOut, int inIdVersion,
          string stAttrName, string vaAttrValue, string stIdUnit,
          bool boDel);
        bool UpAttrValueById(int idCheckOut, int inIdVersion,
          string stAttrName, DateTime vaAttrValue, string stIdUnit,
          bool boDel);
        bool UpAttrValueById(int idCheckOut, int inIdVersion,
          string stAttrName, double vaAttrValue, string stIdUnit,
          bool boDel);
        // перегруженные методы, отличаются типом параметра vaAttrValue
        bool UpLinkAttrValue(int idCheckOut, int inIdLink, string stAttrName,
          int vaAttrValue, string stIdUnit, bool boDel);
        bool UpLinkAttrValue(int idCheckOut, int inIdLink, string stAttrName,
          string vaAttrValue, string stIdUnit, bool boDel);
        bool UpLinkAttrValue(int idCheckOut, int inIdLink, string stAttrName,
          DateTime vaAttrValue, string stIdUnit, bool boDel);
        bool UpLinkAttrValue(int idCheckOut, int inIdLink, string stAttrName,
          double vaAttrValue, string stIdUnit, bool boDel);
        bool GetDbList(out string res);
		
		// API-клиента ----------------------
		bool GetSignatures(int aIdCheckout, int aVersionId, out IPluginDataSet ds);
		bool GetAnnotations(int aIdCheckout, int aVersionId, out IPluginDataSet ds);
		
		bool SetUserSetPattern(string userSetName, string dbName, string xmlPattern);
		bool UpUserSet(string stUserSet, string stDbName, string stNewUserSet, string stGroupName,
      int inGroupOperation, bool boDel);
		// ----------------------------------
		
		bool GetReport(int idCheckOut, string reportName,
          string Params, out IPluginDataSet ds);

        /// <summary>Метод создает новую версию объекта.</summary>
        /// <param name="idCheckOut"></param>
        /// <param name="stTypeName">Название типа.</param>
        /// <param name="stProductName">Ключевой атрибут.</param>
        /// <param name="stVersionNumber">Версия объекта.</param>
        /// <param name="stStateName">Состояние новой версии.</param>
        /// <param name="vaFileList">Список файлов, которые должны быть унаследованы новой версией. Указывается
        ///  ввиде строки с разделителями "," и "|". Пример для 2-х файлов:
        ///  'file1.png, 5434\, file1.png, -5555\|file1.rtf, , file1.rtf, -55555\'
        ///  или пример для одного файла:
        ///  'file1.png, 5434\, file1.png, -5555\'
        ///  если копировать вайлы в новую версию не требуется то передается пустая строка.
        /// </param>
        /// <param name="inGroup">Позиция разряда в номере версии.</param>
        /// <param name="inParams">Параметры.</param>
        /// <param name="res">возвращает идентификатор созданной версии..</param>
        bool NewVersionEx(int idCheckOut, string stTypeName, string stProductName,
          string stVersionNumber, string stStateName, string vaFileList, int inGroup,
          int inParams, out int res);		
		
		/// <summary>Метод скачивает файл с сервера ЛОЦМАН в указанный TargetFile,
		/// минуя кэш.
		/// Внимание! Метод Loodsman_ExtractFile не использует кэширование файлов
		/// поэтому в большинстве случаев рекомендуется использовать метод Client_GetFileById
		/// </summary>
		bool Loodsman_ExtractFile(int idCheckout, string typeObj, string product,
		  string version, int idVersion, 
          string stFileName, string stLocalName, int inSize, int inCrc, DateTime dtCreate, DateTime dtModify, 
          string targetFile);
		
        bool PrintFileToXps(string fileName, string xpsFileName);
		// не реализованные методы
        /*bool GrantRightsForObjects(objects: Variant;
          out IPluginDataSet ds);*/
		/*bool CheckOutLicense(string hostId, int feature, string description, int options);*/
    }


    [Guid("742671fa-f3ef-4071-89c1-466a6ccaadd1")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginAppWorkFlow
    {
        string GetLastError();
        
        // API-клиента
        bool CreateTask(string subj, string text, int idVersion,
            int idRole, int idUser, DateTime datePlan, int criticalTerm, bool checkResult,
            TbProcessKindExp kind, out int idRoute);
        bool StartTask(int idRoute, int idRole, int idUser,
            string subj, DateTime datePlan);
        bool CreateTaskByDialog(out int idRoute);
        
        // API-ЛОЦМАН WorkFlow
        bool GetIncomingCorrespondence(int inRetMode, out IPluginDataSet ds);
        bool GetOutcomingCorrespondence(int inRetMode, out IPluginDataSet ds);
        bool GetCorrespondenceList(int inIdRoute, out IPluginDataSet ds);
        bool DelLetter(int inIdLetter);
        bool GetUserList(out IPluginDataSet ds);
        bool GetRoleTree(int inMode, out IPluginDataSet ds);
        bool GetProcessListByObject(string stDbName, int inPdmId, out IPluginDataSet ds);
        bool GetProcessListByObjectEx(int inPdmId, string stDbName, out IPluginDataSet ds);
        bool GetInitialProcessList(out IPluginDataSet ds);
        bool GetInfoAboutCurrentUser(out IPluginDataSet ds);
        bool GetInfoAboutRoute(int inIdRoute, out IPluginDataSet ds);
        bool GetRoleListForUser(int inIdUser, out IPluginDataSet ds);
        bool GetTaskListForUser(int inIdUser, int inUserRoleMask,
            int inProcessStateMask, int inStageStateMode, out IPluginDataSet ds);
        bool CreateNewProcess(string stDbName, int inIdPdmObject,
            string stRouteName, out int res);
        bool DeleteProcess(int inIdRoute);
        bool InsUpdStageIntoBp(string stTask,
            int inCriticalTerm, DateTime dtDatePlan, int inCanControl, int inActive,
            int inIdRole, int inIdUser, int inIdBProc, int inCanTransform, ref int inIdStage,
            ref int inTimeConstraint, ref int inFromBpStart);
        bool AddRouteItemsSeq(int inIdFirst, int inIdSecond, int inType);
        bool AddRouteItemsRetSeq(int inIdFirst, int inIdSecond);
        bool GetRoleListIntoProcess(int inIdRoute, out IPluginDataSet ds);
        bool SetStateOfProcess(int inIdRoute, int inStateBp);
        bool InsUpdUser(string stUserName, string stDescr, string stEMail, int inIsAdmin, 
            ref int vaIdUser);
        bool SetTaskState(int inIdRoute, int inIdOperation, int inStateTask, 
            string stMessageBody);
        bool SetItemState(int inIdItem, int inState, int inAddSignature);
        bool SetItemStateInternal(int inIdItem, int inState);
        bool AddBpDocument(int inIdRoute, int inIdDoc, int inIdPar);
        bool DelBpDocument(int inIdRoute, int inIdDoc);
        bool GetFirstItems(int inIdRoute, out IPluginDataSet ds);
        bool GetNextItems(int inIdItem, out IPluginDataSet ds);
        bool rep_ExecutedJobs(int inIdParent, out IPluginDataSet ds);
        bool GetJobsListIntoProcess(int inIdRoute, out IPluginDataSet ds);
        bool GetAttachDocumentListIntoProcess(int inIdRoute, out IPluginDataSet ds);
        bool StartNextStage(int inIdRoute, int inIdItem, int inDirect);
        bool RenameProcess(int inIdRoute, string stNewName);
        bool SetStageExecutor(int inIdStage, int inIdUser, int inIdRole);
        bool SetStageTerm(int inIdStage, int inTimeLimit,
            DateTime vaPlanEnd, int inTimeConstraint, int inFromBpStart);
        bool SetStageTask(int inIdStage, string stTask);
        bool SetBP_PDMObject(int inIdRoute, int inIdPdmObject);
        bool GetTaskListForObject(int inIdVersion, int inProcessStateMask,
            int inStageStateMode, out IPluginDataSet ds);
        bool GetInfoAboutStage(int inIdStage, out IPluginDataSet ds); 
        bool SendLetterByBpToUsers(int inIdRoute, int inIdAuRole, 
            IPluginDataSet vaRecipientList, string stSubj, string stBody, out int idLetter);

		bool GetVariable(int inIdRoute, string stName, out int res);
        bool GetVariable(int inIdRoute, string stName, out string res);
        bool GetVariable(int inIdRoute, string stName, out DateTime res);
        bool GetVariable(int inIdRoute, string stName, out double res);
        bool SetVariable(int inIdRoute, string stName, int value, bool readOnlyForChild);
        bool SetVariable(int inIdRoute, string stName, string value, bool readOnlyForChild);
        bool SetVariable(int inIdRoute, string stName, DateTime value, bool readOnlyForChild);
        bool SetVariable(int inIdRoute, string stName, double value, bool readOnlyForChild);
		
        bool SendLetterByBpToOneUser(int inIdRoute, int inIdAuRole, int inIdRecRole, int inIdRecUser,
               string stSubj, string stBody, out int res);
		
		bool GetTypicalProcessList(int inMode, out IPluginDataSet ds);
        bool CreateNewProcessBy(string stDbName, int inIdPdmObject, string stRouteName, int inIdBaseRoute, out int res);
		bool InsUpdOpInstance(int inIdOperDescr, int inIsWait, int inIdProc, int inIdOperInst, out int inIdNew);
		bool GetPrevItems(int inIdItem, out IPluginDataSet ds);
		
        // не реализованные методы
        //    procedure SetLetterRead(inIDLetter: Integer; inIdRole: Variant;
        //      inIsRead: integer);
        //    function SetXMLImageToProcess(inID_Route: Integer; const stXML: string): Variant;
        //    procedure InsUpdAutoOperation(stName: string; inType, inIsAccess,
        //      inIsSystem: Integer; stScript: Variant; var vaIdOper: Variant);
    }


	[Guid("fe6c33c2-f27d-4b2f-9c84-c39119ac482a")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginAppConfigurator
    {
        string GetLastError();
        
		// API-ЛОЦМАН Конфигуратор
        bool GetSortCase(string stProfile, string stType, string stLink,
          int inIndex, out IPluginDataSet ds);
        bool GetSortTypes(int inIdCase, out IPluginDataSet ds);
        bool GetSortTree(int inIdCase, out IPluginDataSet ds);
        bool GetSortTable(int inIdCase, out IPluginDataSet ds);
	}
	
	
    [Guid("2b739fcc-2c99-429e-8bc8-7fbed03c8832")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginPdmVersion
    {
        int IdVersion { get; }
        int IdCheckout { get; }
        AccessLevelExp AccessLevel { get; } 
        string Product { get; }
        string TypeObj { get; }
        string Version { get; }
        string State { get; }
		string Title();
		string TitleAttrValue(string name); 
    }

    
    [Guid("0b456eb1-794d-4796-bb9f-63ec63dbf8ca")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginPdmVersions
    {
        int Count { get; }
        IPluginPdmVersion Version(int index);
    }


    [Guid("2869f541-d50e-46a1-a704-91bfce5e04d2")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginAppProvider
    {
        string GetAppTitle();
        int GetAppHandle();
        IPluginAppMain GetAppMain();
        IPluginAppWorkFlow GetAppWorkFlow();
        IPluginAppCache GetAppCache();
        IPluginPdmVersions GetSelectedObjects();
        void RefreshVersion(int idVersion);
        bool SelectObject(int idVersion);
        bool SelectTask(int idProcess, TasksFilterExp tasksFilter);
        bool SelectMail(int idLetter, MailFilterExp filter);
		/// <summary>Возвращает интерфейс "API-ЛОЦМАН Конфигуратор".</summary>
        /// <returns>Интерфейс "API-ЛОЦМАН Конфигуратор".</returns>
        IPluginAppConfigurator GetAppConfigurator();

        bool GetSelectedTasks(out IPluginTasks res);
        bool GetAttachFromDlg(out IPluginPdmVersions versions, out ModalResultExp res);
        bool GetUsersFromDlg(bool aDateVisible, bool aSignRoleVisible, bool aMultiselect, bool aCanIAm, 
                IPluginUsers aSelectedUsers, out IPluginUsers user, out ModalResultExp res);
        bool RefreshTasks(int aSelectedRouteId);

        string GetLastError();
    }


    [Guid("bd5e8076-4e5f-44de-8f29-1206e7cd48a5")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginCoolCommandsPanel
    {
        /// <summary>Рекомендуется использовать AddCommand2. Команда AddCommand оставлена для
        /// совместимости с предыдущей версией SDK.</summary>
        void AddCommand(string aCommandName, string aGroupName, string aImageName, int aIndex);
        void ReinitCoolCommands();
        /// <summary>Добавляет команду в указанный спейс и указанную группу. Если указать существующую системную группу и системное наименование
        /// команды то произойдет замена системной команды.</summary>
        void AddCommand2(string aSpaceName, string aGroupName, string aCommandName, string aImageName, int aIndex);
    }

  
    [Guid("2dcb409a-f486-4ae1-81bb-c8bfca4f0b8b")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginOpenSpacePanel
    {
        void AddOpenSpace(string aCaption, string aLeftPanelClassName, string aRightPanelClassName);
    }

	[Guid("a26cd0d3-f799-4a6d-83ed-cd7e3c98131e")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginMainMenu
    {
        void AddCommand(string aCaption, int aIndex);
    }
	

    //-------------------------------------------------------
    // Интерфейсы реализуемые плагином
    //-------------------------------------------------------

    // Интерфейс обязательный для реализации в любом плагине
    [Guid("087c9bc0-fefd-471e-acef-3ef0e0ba9267")]
    public interface IPlugin : IHYCrossPlatformInterface
    {
        int GetVersion();
        string GetLastError();
        void Init(IPluginAppProvider appProvider);
    }


    [Guid("608f0f4e-b00f-49ac-8ae2-2bcf6815d9f3")]
    public interface IPluginCoolCommands : IHYCrossPlatformInterface 
    {
        void InitCoolCommands(IPluginCoolCommandsPanel accPanel); 
        void CallCoolCommand(int aIndex); 
    }

    
    [Guid("2c049382-c607-4ba0-ba05-6b7d607d8b21")]
    public interface IPluginOpenSpace : IHYCrossPlatformInterface
    {
        void InitOpenSpace(IPluginOpenSpacePanel aPanel);
    }

	// Интерфейс для перехвата различных событий главного приложения
    [Guid("641e6182-befa-4ddd-814e-99e7b6dfa02c")]
    public interface IPluginEvents : IHYCrossPlatformInterface
    {
        // Метод AfterCreateDocument вызывается после создания документа, перед регистрацией файлов
        CallResult AfterCreateDocument(IPluginPdmVersion aDocument, string aFile);
        // Метод AfterEditObject вызывается после редактирования объекта в диалоге свойств, перед CheckIn
        CallResult AfterEditObject(IPluginPdmVersion aParent, IPluginPdmVersion aObject, int aIdLink,
            string aLinkType, string aNewProduct, string aNewState);
        CallResult CheckAttachment(IPluginPdmVersion version, out string rejectReason);
    }
	
	
	[Guid("ebe20987-48e5-4e33-8664-66ac259f05c1")]
	public interface IPluginMenu : IHYCrossPlatformInterface
	{
		void InitMainMenu(IPluginMainMenu aMenu); 
		void CallMainMenu(int aIndex); 
	}

    [Guid("6b8a03e9-786e-4868-9a7c-f5ce408d55f8")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginUser
    {
        int IdUser { get; }
        string UserName { get; }
        string FullUserName { get; }
        int IdRole { get; }
        string RoleName { get; }
        DateTime PlanDate { get; }
        string SignRole { get; }
    }

    [Guid("1cfa5311-9369-4c48-9ea7-f5e6b7a34114")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginTask
    {
        int OperationId { get; }
        string Text { get; }
        string Subject { get; }
        DateTime CreateDate { get; }
        DateTime CompleteDate { get; }
        string Author { get; }
        string AuthorFullName { get; }
        IPluginUser Executor { get; }
        int RouteId { get; }
        TaskStateExp State { get; }
        TaskKindExp Kind { get; }
        bool HasAttachments { get; }
        bool Accepted { get; }
        TbProcessKindExp ProcessKind { get; }
        int IdVersion { get; }
    }

    [Guid("f98f70cf-d65d-4371-941a-7f05492efe4b")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginTasks
    {
        int Count { get; }
        IPluginTask Task(int index);
    }

    [Guid("05dfeb40-9fac-4b98-aa2a-dc054c4deb54")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPluginUsers
    {
        int Count { get; }
        IPluginUser User(int index);
    }

    class PluginConsts
    {
        public const int PluginVersion = 20120214;
        
        // Наименования спейсов для добавления команд
        public const string CSpaceDocuments = "SpaceDocuments";
        public const string CSpaceTasks = "SpaceTasks";
        // Наименования групп для добавления команд
        public const string CGroupObject = "Задачи для документов и папок";
        public const string CGroupDetail = "Подробно";
        public const string CGroupProject = "Задачи для проекта";
        public const string CGroupTask = "Работа с заданием";
    }
    
}
