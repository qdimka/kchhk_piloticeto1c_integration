﻿using Newtonsoft.Json;

namespace LinkPilotIceTo1C.Common.Settings
{
    public class Settings
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string ConnectionString { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string DataBase { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string Password { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string UserName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string IdVersion { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string PathToLog  { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string Types { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string PathToPilot { get; set; }
    }
}
