﻿using LinkPilotIceTo1C.ExternalClient.Service;
using System;
using System.Windows.Forms;

namespace wfaTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //[DllImport("LoodsmanPGSDLL.dll")]
        //public static extern void ResPath([MarshalAs(UnmanagedType.BStr)] out string s);

        private void button1_Click(object sender, EventArgs e)
        {
            var service = ExternalService.Service;
            service.StringAvalaible += (s, ev) => {
                MessageBox.Show((ev as StringAvalaibleEventArgs).PilotUrl);
            };

            service.Start(textBox1.Text);
        }
    }
}
