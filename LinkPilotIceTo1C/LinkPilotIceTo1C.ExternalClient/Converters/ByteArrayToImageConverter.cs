﻿using LinkPilotIceTo1C.ExternalClient.ViewModels;
using SharpVectors.Runtime;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace LinkPilotIceTo1C.ExternalClient.Converters
{
    public class ByteArrayToImageConverter : IValueConverter
    {
        // Methods
        public static DrawingImage Convert(byte[] bytes)
        {
            DrawingGroup group;
            if ((bytes == null) || (bytes.Length == 0))
            {
                return null;
            }
            if (!SvgConverterHelper.TryConvert(bytes, out group))
            {
                return null;
            }
            var drawingRect = GetDrawingRect(group);
            var brush = new DrawingBrush(group)
            {
                Stretch = Stretch.None,
                Viewbox = drawingRect,
                ViewboxUnits = BrushMappingMode.Absolute
            };
            return new DrawingImage(new GeometryDrawing
            {
                Brush = brush,
                Geometry = new RectangleGeometry(drawingRect)
            });
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(ImageSource))
            {
                throw new InvalidOperationException("The target must be ImageSource or derived types");
            }
            var bytes = value as byte[];
            if (bytes != null)
            {
                return Convert(bytes);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private static Rect GetDrawingRect(DrawingGroup rootGroup)
        {
            var drawingRectRecursive = GetDrawingRectRecursive(rootGroup);
            if (!drawingRectRecursive.HasValue)
            {
                throw new InvalidOperationException("Ошибка при попытке получить размеры drawing group");
            }
            return drawingRectRecursive.Value;
        }

        private static Rect? GetDrawingRectRecursive(DrawingGroup group)
        {
            if (SvgLink.GetKey(group) == "DrawingLayer")
            {
                var clipGeometry = group.ClipGeometry as RectangleGeometry;
                if (clipGeometry != null)
                {
                    return new Rect?(clipGeometry.Bounds);
                }
            }
            foreach (var group2 in group.Children.OfType<DrawingGroup>())
            {
                var drawingRectRecursive = GetDrawingRectRecursive(group2);
                if (drawingRectRecursive.HasValue)
                {
                    return drawingRectRecursive;
                }
            }
            return null;
        }
    }

}
