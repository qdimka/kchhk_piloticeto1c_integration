﻿using LinkPilotIceTo1C.ExternalClient.ViewModels;
using System;
using System.Windows;

namespace LinkPilotIceTo1C.ExternalClient.Views
{
    /// <summary>
    /// Логика взаимодействия для RootView.xaml
    /// </summary>
    public partial class RootView : Window
    {
        public RootView()
        {
            InitializeComponent();
        }

        public RootViewModel RootViewModel
        {
            get => this.DataContext as RootViewModel;
            set
            {
                this.DataContext = value;
                //TODO Получить указатель на закрытие окна
                if (RootViewModel.Close == null)
                    RootViewModel.Close = new Action(() => this.Close());
            }
        }
    }
}
