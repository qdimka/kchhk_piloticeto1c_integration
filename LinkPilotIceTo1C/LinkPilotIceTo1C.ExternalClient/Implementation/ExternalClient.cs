﻿using LinkPilotIceTo1C.ExternalClient.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using LinkPilotIceTo1C.ExternalClient.ViewModels;
using LinkPilotIceTo1C.ExternalClient.Models;
using Pilot.Core;
using Pilot.Server.Api;
using Pilot.Server.Api.Contracts;
using LinkPilotIceTo1C.Common.Settings;

namespace LinkPilotIceTo1C.ExternalClient.Implementation
{
    public class ExternalPilotClient
    {
        #region fields

        private static readonly ExternalPilotClient Client = new ExternalPilotClient();
        private HttpPilotClient _pilotClient;
        private IServerApi _api;
        private DDatabaseInfo _dbInfo;
        private List<MType> _types;
        private readonly Logger _logger = Logger.GetInstance();
        private Settings _settings;

        #endregion

        #region properties

        public static ExternalPilotClient GetInstance() => Client;
        public bool IsConnected { get; private set; } = false;

        #endregion

        public void Connect(Settings settings)
        {
            if (IsConnected)
                return;

            _settings = settings;

            try
            {
                _pilotClient = new HttpPilotClient();

                _pilotClient.Connect(_settings.ConnectionString);

                _api = _pilotClient.GetServerApi(new ExternalCallback());

                _dbInfo = _api.OpenDatabase(settings.DataBase, settings.UserName, settings.Password, false, 103);

                if (_dbInfo != null)
                    IsConnected = true;

                _types = _api.GetMetadata(_dbInfo.MetadataVersion).Types;
            }
            catch (Exception e)
            {
                _logger.Write(e.Source, $"При подключении к серверу Pilot-Ice возикло исключение: {e.Message}");
            }
        }

        public void DisConnect()
        {
            try
            {
                if (IsConnected)
                {
                    _pilotClient.Disconnect();
                    IsConnected = false;
                }
            }
            catch (Exception e)
            {
                _logger.Write(e.Source, $"При отключении от сервера Pilot-Ice возикло исключение: {e.Message}");
            }
        }

        public IExternalCallback ExternalCallback 
            => _pilotClient
            .GetImplementation("IServerCallback") as ExternalCallback;

        private string DisplayName(DObject obj) 
            => obj.GetTitle(_types.Find(p => p.Id == obj.TypeId));

        public List<PilotObject> GetPObjects(string context = null)
        {
            var g = Guid.Empty;

            g = context == null ? DObject.RootId : new Guid(context);

            if (_api == null)
                return null;

            var objects = GetDObjects(g);

            return PObjectToList(objects);
        }

        private List<PilotObject> PObjectToList(List<DObject> objects)
        {
            if (objects == null)
                return null;

            var documents = new List<PilotObject>();

            foreach (var item in objects)
            {
                foreach (var child in item.Children)
                {
                    var childObject = _api
                        .GetObjects(new[] { child.ObjectId })?
                        .FirstOrDefault();
                    if(childObject != null)
                        documents.Add(DpObjectToDocument(childObject));
                }
            }

            var filteredList = documents.FindAll(p => Filter(p.Type) && !String.IsNullOrEmpty(p.Name));

            return filteredList?.ToList();
        }

        private PilotObject DpObjectToDocument(DObject obj)
        {
            if (obj == null)
                return null;

            return new PilotObject
            {
                Id = obj.Id.ToString(),
                Attributes = obj.Attributes,
                Name = DisplayName(obj),
                Type = _types.Find(p => p.Id == obj.TypeId).Title,
                Icon = _types.Find(p => p.Id == obj.TypeId).Icon,
                ChildCount = obj.Children.Count,
                ParentId = obj.ParentId.ToString()
            };
        }

        public Stack<Guid> Parents(Guid childId)
        {
            if (childId == Guid.Empty)
                return null;

            var parents = new Stack<Guid>();

            parents.Push(childId);

            var current = childId;

            while (current != DObject.RootId)
            {
                current = GetDObjects(current).First().ParentId;
                if (current != DObject.RootId)
                {
                    parents.Push(current);
                }
            }

            return parents;
        }

        private List<DObject> GetDObjects(Guid context) 
            => _api.GetObjects(new[] { context });

        public bool Filter(string type)
        {
            var flag = true;

            if (_settings.Types != null)
            {
                foreach (var item in _settings.Types?.Split(','))
                {
                    if (!string.IsNullOrEmpty(item) && type.Contains(item))
                    {
                        flag = false;
                    }
                }
            }

            return flag;
        }

        public List<PilotObject> GetPObjectById(Guid[] ids)
        {
            if (ids == null)
                return null;

            var documents = new List<PilotObject>();

            foreach (var item in ids)
            {
                var obj = _api.GetObjects(new[] { item }).FirstOrDefault();
                if (obj != null)
                {
                    documents.Add(DpObjectToDocument(obj));
                }
            }

            return documents
                .FindAll(p => Filter(p.Type) && !string.IsNullOrEmpty(p.Name));
        }

        public Guid AddSearch(string searchRequest)
        {
            var def = new DSearchDefinition
            {
                SearchString = searchRequest,
                ContextObjectId = DObject.RootId,
                SearchKind = SearchKind.FullText,
                MaxResults = 250
            };

            _api.AddSearch(def);
            return def.Id;
        }

        public Guid AddCustomSearch(string attributeName, string attributeValue)
        {
            var searchRequest = $"+{attributeName}:\"{attributeValue}\"";

            var def = new DSearchDefinition
            {
                SearchString = searchRequest,
                ContextObjectId = DObject.RootId,
                SearchKind = SearchKind.Custom,
                MaxResults = 250
            };

            _api.AddSearch(def);
            return def.Id;
        }

        public void RemoveSearch(Guid id)
        {
            _api.RemoveSearch(id);
        }

        public PilotFile GetXpsDocument(string id)
        {
            if (String.IsNullOrEmpty(id))
                return null;

            var obj = _api.GetObjects(new[] { new Guid(id) })?.First();
            var files = obj?.ActualFileSnapshot?.Files;

            if (files?.Count == 0)
                return null;

            var file = obj?
                .ActualFileSnapshot?
                .Files?
                .First();

            return new PilotFile()
            {
                FileName = file?.Name,
                FileBody = _api?.GetFileChunk(file.Body.Id, 0, (int)file.Body.Size)
            }; ;
        }
    }
}
