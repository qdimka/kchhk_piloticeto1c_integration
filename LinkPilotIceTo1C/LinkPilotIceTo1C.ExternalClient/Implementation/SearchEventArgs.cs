﻿using System;
using Pilot.Core;

namespace LinkPilotIceTo1C.ExternalClient.Implementation
{
    public class SearchEventArgs:EventArgs
    {
        public DSearchResult SearchResult { get; set; }
    }
}
