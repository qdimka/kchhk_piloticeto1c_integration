﻿using LinkPilotIceTo1C.ExternalClient.Infrastructure;
using System;
using Pilot.Core;

namespace LinkPilotIceTo1C.ExternalClient.Implementation
{
    public class ExternalCallback : IExternalCallback
    {
        public event EventHandler SearchComplete;

        public void NotifyChangeset(DChangeset changeset)
        {
            throw new NotImplementedException();
        }

        public void NotifyDMetadataChangeset(DMetadataChangeset changeset)
        {
            throw new NotImplementedException();
        }

        public void NotifyDNotificationChangeset(DNotificationChangeset changeset)
        {
            throw new NotImplementedException();
        }

        public void NotifyGeometrySearchResult(DGeometrySearchResult searchResult)
        {
            throw new NotImplementedException();
        }

        public void NotifyOrganisationUnitChangeset(OrganisationUnitChangeset changeset)
        {
            throw new NotImplementedException();
        }

        public void NotifyPersonChangeset(PersonChangeset changeset)
        {
            throw new NotImplementedException();
        }

        public void NotifySearchResult(DSearchResult searchResult)
        {
            OnSearchComplete(new SearchEventArgs(){ SearchResult = searchResult});
        }

        protected virtual void OnSearchComplete(SearchEventArgs e)
        {
            SearchComplete?.Invoke(this, e);
        }
    }
}
