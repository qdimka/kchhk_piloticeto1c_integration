﻿using LinkPilotIceTo1C.ExternalClient.Implementation;
using LinkPilotIceTo1C.ExternalClient.Models;
using LinkPilotIceTo1C.ExternalClient.Utils.Mediator;
using MsAccessViewer.Viewer.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public class TreeViewModel : ViewModelBase
    {
        private ExternalPilotClient _client = ExternalPilotClient.GetInstance();

        private ObservableCollection<DocumentViewModel> _documentsViewModels;
        private DocumentViewModel _selectedItem;
        private ICommand _selectCommand;

        public TreeViewModel(List<PilotObject> objects, MessengerServiceBase messenger,string link) :base(messenger)
        {
            this._documentsViewModels = new ObservableCollection<DocumentViewModel>(
                    objects.Select(x => new DocumentViewModel(x,this.Messenger))
                );
             //Если передана ссылка
            if (link != null)
            {
                if (link.Length >= "piloturi://a1d7ec5f-9683-4e54-98b3-cabf2ccc27d1".Length)
                {
                    var id = link.Substring(link.IndexOf("/") + 2, 36);
                    ExpandTreeView(id);
                }
            }

            _selectCommand = new DelegateCommand(Select, () => SelectedItem.Obj != null && SelectedItem.Obj.Type.Contains("Документ"));
        }

        public ObservableCollection<DocumentViewModel> DocumentsViewModels
        {
            get => _documentsViewModels;
            set
            {
                _documentsViewModels = value;
                NotifyPropertyChanged("DocumentsViewModels");
            }
        }

        public DocumentViewModel SelectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;
                NotifyPropertyChanged("Selected");

                this.Messenger.Send(_selectedItem.Obj, typeof(AttributesViewModel).ToString());
            }
        }

        public ICommand SelectCommand => _selectCommand;

        private void Select()
        {
            //Отправим выбранный объект корневой VM
            this.Messenger.Send(SelectedItem.Obj.Link, typeof(RootViewModel).ToString());
        }

        public void ExpandTreeView(string id)
        {
            var document = _client.GetPObjectById(new Guid[] { new Guid(id) }).FirstOrDefault();

            if (document != null)
            {
                var parents = _client.Parents(new Guid(id));

                //Если объект не найден, то в стеке будет только один id
                var objects =new  ObservableCollection<DocumentViewModel>(
                    _client.GetPObjects().Select(x => new DocumentViewModel(x, this.Messenger))
                    );

                var objectId = parents.Pop();
                var obj = objects.FirstOrDefault(p => p.ObjId == objectId.ToString());

                obj = ExpandBeforeDocument(obj, parents);

                objects[objects.IndexOf(obj)] = obj;
                DocumentsViewModels = objects;
            }
        }

        private DocumentViewModel ExpandBeforeDocument(DocumentViewModel children, Stack<Guid> parents)
        {
            if (children == null)
            {
                return null;
            }

            children.IsExpanded = true;

            if (parents.Count == 0)
            {
                children.IsSelected = true;
                return children;
            }

            var id = parents.Pop().ToString();
            var next = children.Children.FirstOrDefault(p => (p as DocumentViewModel)?.ObjId == id);

            if (next != null)
            {
                next = ExpandBeforeDocument((next as DocumentViewModel), parents);
            }

            children.Children[children.Children.IndexOf(next)] = next;
            return children;
        }

        public override void Notify(object message)
        {
            ExpandTreeView((message as PilotObject).Id);
        }
    }
}
