﻿using LinkPilotIceTo1C.ExternalClient.Implementation;
using LinkPilotIceTo1C.ExternalClient.Utils.Mediator;
using System;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public class RootViewModel : ViewModelBase
    {
        private TreeViewModel _treeViewModel;

        private SearchViewModel _searchViewModel;

        private AttributesViewModel _attributesViewModel;

        private DocumentViewModel _documentViewModel;
        private string _pilotUrl;

        public RootViewModel(MessengerServiceBase messenger,
                            TreeViewModel treeViewModel,
                            SearchViewModel searchViewModel,
                            AttributesViewModel attributesViewModel,
                            DocumentViewModel documentViewModel,
                            string link):
            base(messenger)
        {
            this._treeViewModel = treeViewModel;
            this._searchViewModel = searchViewModel;
            this._attributesViewModel = attributesViewModel;
            this._documentViewModel = documentViewModel;
        }

        public TreeViewModel TreeViewModel
        {
            get => _treeViewModel;
            set
            {
                _treeViewModel = value;
                NotifyPropertyChanged("TreeViewModel");
            }
        }

        public SearchViewModel SearchViewModel
        {
            get => _searchViewModel;
            set
            {
                _searchViewModel = value;
                NotifyPropertyChanged("SearchViewModel");
            }
        }

        public AttributesViewModel AttributesViewModel
        {
            get => _attributesViewModel;
            set
            {
                _attributesViewModel = value;
                NotifyPropertyChanged("AttributesViewModel");
            }
        }

        public DocumentViewModel DocumentViewModel
        {
            get => _documentViewModel;
            set
            {
                _documentViewModel = value;
                NotifyPropertyChanged("DocumentViewModel");
            }
        }

        public Action Close { get; internal set; }

        public string PilotUrl
        {
            get => _pilotUrl;
            set
            {
                _pilotUrl = value;
                NotifyPropertyChanged("PilotUrl");
            }
        }

        public override void Notify(object message)
        {
            PilotUrl = (message as string);
            ExternalPilotClient.GetInstance().DisConnect();
            Close();
        }
    }
}
