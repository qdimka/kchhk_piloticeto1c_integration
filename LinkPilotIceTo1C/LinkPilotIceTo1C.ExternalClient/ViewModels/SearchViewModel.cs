﻿using LinkPilotIceTo1C.ExternalClient.Implementation;
using LinkPilotIceTo1C.ExternalClient.Models;
using LinkPilotIceTo1C.ExternalClient.Utils.Mediator;
using MsAccessViewer.Viewer.Utils;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public class SearchViewModel : ViewModelBase
    {
        private ExternalPilotClient _client = ExternalPilotClient.GetInstance();
        private Guid _searchId;

        private ObservableCollection<PilotObject> _searchResults;
        private string _searchRequest;
        private ICommand _searchCommand;
        private ICommand _sendToTreeCommand;
        private PilotObject _selected;

        public SearchViewModel(MessengerServiceBase messenger) : base(messenger)
        {
            //Подписка на событие возникающее при поиске
            _client.ExternalCallback.SearchComplete += (s, e) =>
            {

                var result = _client.GetPObjectById((e as SearchEventArgs)?.SearchResult?.Found?.ToArray());
                if (result != null)
                {
                    SearchResults = new ObservableCollection<PilotObject>(result);
                }
                //Удалим поиск после получения информации
                _client.RemoveSearch(_searchId);
            };
        }

        public ObservableCollection<PilotObject> SearchResults
        {
            get => _searchResults;
            set
            {
                _searchResults = value;
                NotifyPropertyChanged("SearchResults");
            }
        }

        public PilotObject Selected
        {
            get => _selected;
            set
            {
                _selected = value;
                NotifyPropertyChanged("Selected");

                this.Messenger.Send(_selected, typeof(AttributesViewModel).ToString());
            }
        }

        public string SearchRequest
        {
            get => _searchRequest;
            set
            {
                _searchRequest = value;
                NotifyPropertyChanged("SearchRequest");
            }
        }

        public ICommand SearchCommand
            => _searchCommand
               ?? (_searchCommand = new DelegateCommand(Search, () => !String.IsNullOrEmpty(_searchRequest)));

        private void Search()
        {
            _searchId = _client.AddSearch(_searchRequest);
        }

        public ICommand SendToTreeCommand
            => _sendToTreeCommand
               ?? (_sendToTreeCommand = new DelegateCommand<PilotObject>((e) =>
                {
                    this.Messenger.Send(e, typeof(TreeViewModel).ToString());
                }));
    }
}
