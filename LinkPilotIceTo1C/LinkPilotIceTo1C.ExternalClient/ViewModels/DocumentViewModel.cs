﻿using LinkPilotIceTo1C.ExternalClient.Implementation;
using LinkPilotIceTo1C.ExternalClient.Models;
using LinkPilotIceTo1C.ExternalClient.Utils.Mediator;
using MsAccessViewer.Viewer.Utils;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public class DocumentViewModel : TreeViewItemViewModel
    {
        private ExternalPilotClient _client = ExternalPilotClient.GetInstance();

        private PilotObject _obj;

        private ICommand _selectCommand;
        private ICommand _openInPilotCommand;
        private ICommand _openExtCommand;

        public DocumentViewModel(PilotObject obj, MessengerServiceBase messenger) :base(null,true,messenger)
        {
            this._obj = obj;
            this.Messenger = messenger;

            //TODO Добавить тип для проверки доступа комманды
            _selectCommand = new DelegateCommand(Select, () => Obj != null);
            _openInPilotCommand = new DelegateCommand(OpenInPilot);
            _openExtCommand = new DelegateCommand(OpenInExt, () => Obj != null && Obj.Type.Contains("Документ"));
        }

        public string ObjId
        {
            get => _obj.Id;
            set
            {
                _obj.Id = value;
                NotifyPropertyChanged("ObjId");
            }
        }

        public string ObjType
        {
            get => _obj.Type;
            set
            {
                _obj.Type = value;
                NotifyPropertyChanged("ObjType");
            }
        }

        public String ObjName
        {
            get => _obj.Name;
            set
            {
                _obj.Name = value;
                NotifyPropertyChanged("ObjName");
            }
        }

        public byte[] Icon
        {
            get => _obj.Icon;
            set
            {
                _obj.Icon = value;
                NotifyPropertyChanged("Icon");
            }
        }

        public PilotObject Obj
        {
            get => _obj;
            set
            {
                _obj = value;
                NotifyPropertyChanged("Obj");
            }
        }

        public ICommand SelectCommand => _selectCommand;

        public ICommand OpenInPilotCommand => _openInPilotCommand;

        public ICommand OpenInExtCommand => _openExtCommand;

        private void OpenInExt()
        {
            var documentPreview = _client.GetXpsDocument(Obj.Id);

            if (documentPreview == null)
                return;
            //Запишем файл в темп
            var path = $@"{Path.GetTempPath()}{documentPreview?.FileName}";
            File.WriteAllBytes(path, documentPreview?.FileBody);
            //Проверим что файл записан
            if (File.Exists(path))
            {
                Process.Start(path);
            }
        }

        private void OpenInPilot()
        {
            Process.Start($"piloturi://{Obj.Id}");
        }

        private void Select()
        {
            //Отправим выбранный объект корневой VM
            this.Messenger.Send(Obj.Link, typeof(RootViewModel).ToString());
        }

        protected override void LoadChildren()
        {
            foreach (var item in ExternalPilotClient.GetInstance().GetPObjects(_obj.Id))
            {
                base.Children.Add(new DocumentViewModel(item, Messenger));
            }
        }
    }
}
