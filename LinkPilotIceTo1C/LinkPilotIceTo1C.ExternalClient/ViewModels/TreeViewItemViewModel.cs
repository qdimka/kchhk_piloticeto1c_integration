﻿using LinkPilotIceTo1C.ExternalClient.Utils.Mediator;
using System.Collections.ObjectModel;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public class TreeViewItemViewModel : ViewModelBase
    {
        private static readonly TreeViewItemViewModel DummyChild = new TreeViewItemViewModel();

        private readonly ObservableCollection<TreeViewItemViewModel> _children;
        private readonly TreeViewItemViewModel _parent;

        private bool _isExpanded;
        private bool _isSelected;

        protected TreeViewItemViewModel(TreeViewItemViewModel parent, bool lazyLoadChildren,MessengerServiceBase messenger) : base(messenger)
        {
            this._parent = parent;

            _children = new ObservableCollection<TreeViewItemViewModel>();

            if (lazyLoadChildren)
                _children.Add(DummyChild);
        }

        private TreeViewItemViewModel() : base(null)
        {
        }

        public ObservableCollection<TreeViewItemViewModel> Children => _children;

        public bool HasDummyChild => this.Children.Count == 1 && this.Children[0] == DummyChild;

        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                if (value != _isExpanded)
                {
                    _isExpanded = value;
                    NotifyPropertyChanged("IsExpanded");
                }

                if (_isExpanded && _parent != null)
                    _parent.IsExpanded = true;

                if (this.HasDummyChild)
                {
                    this.Children.Remove(DummyChild);
                    this.LoadChildren();
                }
            }
        }

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                }
            }
        }

        protected virtual void LoadChildren()
        {
        }


        public TreeViewItemViewModel Parent => _parent;
    }
}
