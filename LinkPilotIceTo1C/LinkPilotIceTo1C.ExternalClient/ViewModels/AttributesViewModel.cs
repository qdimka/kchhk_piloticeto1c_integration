﻿using LinkPilotIceTo1C.ExternalClient.Models;
using LinkPilotIceTo1C.ExternalClient.Utils.Mediator;
using System.Collections.Generic;
using Pilot.Core;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public class AttributesViewModel : ViewModelBase
    {
        private SortedList<string, DValue> _attributes;

        public AttributesViewModel(MessengerServiceBase messenger):base(messenger)
        {

        }

        public SortedList<string,DValue> Attributes
        {
            get => _attributes;
            set
            {
                _attributes = value;
                NotifyPropertyChanged("Attributes");
            }
        }

        public override void Notify(object message)
        {
            Attributes = (message as PilotObject)?.Attributes;
        }
    }
}
