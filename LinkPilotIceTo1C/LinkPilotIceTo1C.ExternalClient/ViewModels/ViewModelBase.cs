﻿using LinkPilotIceTo1C.ExternalClient.Utils.Mediator;
using System.ComponentModel;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected MessengerServiceBase Messenger;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ViewModelBase(MessengerServiceBase messenger)
        {
            this.Messenger = messenger;
        }

        public virtual void Notify(object message)
        {

        }
    }
}
