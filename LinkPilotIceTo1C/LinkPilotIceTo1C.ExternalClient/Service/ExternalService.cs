﻿using LinkPilotIceTo1C.ExternalClient.Implementation;
using LinkPilotIceTo1C.ExternalClient.Utils.Mediator;
using LinkPilotIceTo1C.ExternalClient.ViewModels;
using LinkPilotIceTo1C.ExternalClient.Views;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using LinkPilotIceTo1C.Common.Settings;
using Newtonsoft.Json.Linq;

namespace LinkPilotIceTo1C.ExternalClient.Service
{
    public class ExternalService
    {
        private readonly string _config = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\settings.json";

        private ExternalPilotClient _client;

        private readonly Logger _logger = Logger.GetInstance();

        private Guid _searchId;

        private static ExternalService _service;

        public event EventHandler StringAvalaible;

        public static ExternalService Service
            => _service 
               ?? (_service = new ExternalService());

        private Settings _settings;

        private ExternalService()
        {

        }

        private void Init()
        {
            try
            {
                _settings = JObject.Parse(
                    File.ReadAllText(_config)
                ).ToObject<Settings>();

                _client = ExternalPilotClient.GetInstance();
                _client.Connect(_settings);
            }
            catch (Exception e)
            {
                _logger.Write(e.Source, $"Ошибка чтения параметров : {e.Message}");
            }
        }

        public void Start(string link)
        {
            _logger.LogTextAppend += (s, o) =>
            {
                AppendLog(o.Text);
            };

            Init();

            if (_client == null || !_client.IsConnected)
                return;

            if (!string.IsNullOrEmpty(link) && link.Contains("pgsp"))
            {
                Search(link);
            }
            else
            {
                OpenView(link);
            }
        }

        //Пробрасываем событие, костыль костыльный
        //todo Рефакторинг
        public void Search(string link)
        {
            var idPgs = link?.Substring(link.LastIndexOf('/') + 1);
            _client.ExternalCallback.SearchComplete += (s, e) =>
            {
                var result = (e as SearchEventArgs)?.SearchResult;
                if (result.Found != null && result.Found.Count() != 0)
                {
                    OnStringAvalaible(new StringAvalaibleEventArgs()
                    {
                        PilotUrl = _client.GetPObjectById(result.Found.ToArray())?.First()?.Link
                    });
                }
                else
                {
                    OnStringAvalaible(new StringAvalaibleEventArgs()
                    {
                        PilotUrl = ""
                    });
                }
                _client.RemoveSearch(_searchId);
                _client.DisConnect();
            };
            _searchId = _client.AddCustomSearch(_settings?.IdVersion, idPgs);
        }

        public void OpenView(string link)
        {
            var messenger = new MessengerService();

            var objects = ExternalPilotClient.GetInstance().GetPObjects();

            var treeViewModel = new TreeViewModel(objects, messenger, link);
            var searchViewModel = new SearchViewModel(messenger);
            var attributesViewModel = new AttributesViewModel(messenger);
            var documentViewModel = new DocumentViewModel(null, messenger);

            var rootViewModel = new RootViewModel(
                messenger, treeViewModel,
                searchViewModel, attributesViewModel,
                documentViewModel, null
                );

            messenger.Colleagues.Add(treeViewModel.GetType().ToString(), treeViewModel);
            messenger.Colleagues.Add(searchViewModel.GetType().ToString(), searchViewModel);
            messenger.Colleagues.Add(attributesViewModel.GetType().ToString(), attributesViewModel);
            messenger.Colleagues.Add(documentViewModel.GetType().ToString(), documentViewModel);
            messenger.Colleagues.Add(rootViewModel.GetType().ToString(), rootViewModel);

            var v = new RootView { RootViewModel = rootViewModel };
            v.ShowDialog();
            OnStringAvalaible(new StringAvalaibleEventArgs() { PilotUrl = rootViewModel.PilotUrl });
        }

        private void AppendLog(string text)
        {
            if (_settings?.PathToLog == null)
                return;

            File.AppendAllText($"{_settings.PathToLog}", $"{text} \n", Encoding.GetEncoding("UTF-8"));

        }

        public virtual void OnStringAvalaible(StringAvalaibleEventArgs e)
        {
            StringAvalaible?.Invoke(this, e);
        }
    }

    public class StringAvalaibleEventArgs : EventArgs
    {
        public string PilotUrl { get; set; }
    }
}
