﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Pilot.Core;

namespace LinkPilotIceTo1C.ExternalClient.Models
{
    public class PilotObject
    {
        public string Id { get; set; }

        public string ParentId { get; set; }

        public string Name { get; set; }

        public string Link => $"{Name}::piloturi://{Id}";

        public string Type { get; set; }

        public byte[] Icon { get; set; }

        public int ChildCount { get; set; }

        public SortedList<string, DValue> Attributes { get; set; }

        //Коллекция детей с фейковой нодой
        public ObservableCollection<PilotObject> Children
        {
            get
            {
                if (_children == null)
                    return null;

                if (_children.Count != 0)
                    return _children;

                if (this.ChildCount == 0)
                    return _children;

                _children.Add(new PilotObject { Name = "Загрузка" });
                return _children;
            }
            set => _children = value;
        }

        private ObservableCollection<PilotObject> _children;
    }
}
