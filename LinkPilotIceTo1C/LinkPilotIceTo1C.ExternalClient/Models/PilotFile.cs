﻿namespace LinkPilotIceTo1C.ExternalClient.Models
{
    public class PilotFile
    {
        public string FileName { get; set; }

        public byte[] FileBody { get; set; }
    }
}
