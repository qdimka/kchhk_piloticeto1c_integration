﻿using SharpVectors.Converters;
using SharpVectors.Renderers.Wpf;
using System;
using System.IO;
using System.Windows.Media;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public class SvgConverterHelper
    {
        // Methods
        public static bool TryConvert(byte[] bytes, out DrawingGroup drawing)
        {
            drawing = null;
            if ((bytes == null) || (bytes.Length == 0))
            {
                return false;
            }
            var svgStream = new MemoryStream(bytes);
            try
            {
                var settings = new WpfDrawingSettings
                {
                    IncludeRuntime = true,
                    TextAsGeometry = false
                };
                drawing = new FileSvgReader(settings).Read(svgStream);
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (svgStream != null)
                {
                    svgStream.Dispose();
                }
            }
            return true;
        }
    }



}
