﻿using System;
using System.Collections.Generic;

namespace LinkPilotIceTo1C.ExternalClient.ViewModels
{
    public class Logger
    {
        public delegate void LogTextAppendHandler(object sender, LogTextAppendEventsArgs e);
        public event LogTextAppendHandler LogTextAppend;

        private static readonly Logger Instance = new Logger();
        private List<Error> _log;

        struct Error
        {
            public string Object { get; set; }
            public string Message { get; set; }
            public string Time { get; set; }
        }

        private Logger()
        {
            _log = new List<Error>();
        }

        public static Logger GetInstance()
        {
            return Instance;
        }

        public void Save(string path)
        {
            using (var file = new System.IO.StreamWriter(path))
            {
                foreach (var l in _log)
                {
                    file.WriteLine($"{l.Time} \t {l.Object} \t {l.Message}");
                }
            }
        }

        public void Write(string o = "", string m = "")
        {
            var time = DateTime.Now.ToString();

            _log.Add(new Error
            {
                Object = o,
                Message = m,
                Time = time
            });

            OnAppendText(new LogTextAppendEventsArgs
            {
                Text = $"{time} {o} {m}",
            });
        }

        protected virtual void OnAppendText(LogTextAppendEventsArgs e)
        {
            LogTextAppend?.Invoke(this, e);
        }
    }

    public class LogTextAppendEventsArgs : EventArgs
    {
        public string Text;
    }
}

