﻿using System;

namespace LinkPilotIceTo1C.ExternalClient.Utils.Mediator
{
    public abstract class MessengerServiceBase
    {
        public abstract void Send(Object message, string type);
    }
}
