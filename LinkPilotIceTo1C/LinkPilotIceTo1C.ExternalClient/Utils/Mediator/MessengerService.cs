﻿using LinkPilotIceTo1C.ExternalClient.ViewModels;
using System.Collections.Generic;

namespace LinkPilotIceTo1C.ExternalClient.Utils.Mediator
{
    public class MessengerService : MessengerServiceBase
    {
        public Dictionary<string,ViewModelBase> Colleagues { get; set; } = new Dictionary<string,ViewModelBase>();

        public override void Send(object message, string type)
        {
            var vm = Colleagues[type];
            if (vm != null)
                vm.Notify(message);           
        }
    }
}
