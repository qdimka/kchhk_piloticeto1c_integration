﻿using Pilot.Core;
using Pilot.Server.Api.Contracts;

namespace LinkPilotIceTo1C.ExternalClient.Infrastructure
{
    internal interface IExternalClientCallback:IServerCallback
    {
        /// <summary>
        /// Результаты поиска
        /// </summary>
        /// <returns></returns>
        DSearchResult GetSearchResult();
    }
}
