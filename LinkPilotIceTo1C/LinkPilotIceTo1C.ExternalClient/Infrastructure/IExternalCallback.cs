﻿using System;
using Pilot.Server.Api.Contracts;

namespace LinkPilotIceTo1C.ExternalClient.Infrastructure
{
    public interface IExternalCallback : IServerCallback
    {
        event EventHandler SearchComplete;
    }
}
