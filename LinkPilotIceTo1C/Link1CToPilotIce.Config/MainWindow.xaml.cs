﻿using LinkPilotIceTo1C.Common.Settings;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;
using Link1CToPilotIce.Config.Converters;
using Newtonsoft.Json.Linq;

namespace Link1CToPilotIce.Config
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private string _config = "settings.json";
        private Settings _settings;

        public Settings Settings
        {
            get => _settings;
            set
            {
                _settings = value; 
                NotifyPropertyChanged(nameof(Settings));
            }
        }


        public MainWindow()
        {
            InitializeComponent();

            if (File.Exists(_config))
            {
                _settings = JObject.Parse(
                        File.ReadAllText(_config)
                    )
                    .ToObject<Settings>();

                _settings.Password = "";
            }
            else
            {
                using (File.Create(_config));
                _settings = new Settings();
            }

            this.DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog
            {
                SelectedPath = Path
                .GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            };

            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            if (Directory.Exists(dialog.SelectedPath))
            {
                SaveFile(dialog.SelectedPath);
            }
        }

        public void SaveFile(string path)
        {
            _settings.Password = _settings.Password.EncryptAes();
            File.WriteAllText(Path.Combine(path,_config), JObject.FromObject(_settings).ToString());
        }

        #region Notify

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
