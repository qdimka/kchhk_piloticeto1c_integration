﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Link1CToPilotIce.Config.Converters
{
    public class StringInAesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return "";
            return (value as string)?.DecryptAes();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return "";
            return (value as string)?.EncryptAes();
        }
    }
}
